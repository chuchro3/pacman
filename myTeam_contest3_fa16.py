"""
This bot uses the concept of hierarchical reinforcement learning.

Multiple atomic agents were trained using Q-learning,
which allowed for simple and obvious reward functions.

List of trained atomic agents: food collector, capsule collector, returner, tagger, defense anticipator

An offensive/defensive parent agent evaluates a score for an action based on a simple decision tree 
and also by comparing corresponding children agents' scores for that action.

At the top, a root agent evaluates a score for an action based on a simple deicision tree
and also by comparing offensive and defensive scores for that action.

Distance observations were used to apply HMM inference to estimate the opponent's location. 
Additional observations included noticing a food/power pellet being eaten (one agent has to be at that exact coordinate)

Communication between agents was used to avoid engaging in identical subtasks (i.e going for the same power capsule)
The inference calculations to determine the opponent's locations was also shared between agents, allowing two observations per turn cycle


TODO for further exploration of this problem:
- Attempt to train parent agents using pure reinforcement learning. 
    Currently parent agents are a blend of max-q and manually building a decision tree.
- Ideally, we can train the root agent using only the game score as the reward function.

Implented by: Robert Chuchro
"""

# myTeam.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from captureAgents import CaptureAgent
import random, time, util
from game import Directions, Actions, Configuration, Grid
import game
from util import nearestPoint
import math

#################
# Team creation #
#################

#is_training = True 
is_training = False 


def createTeam(firstIndex, secondIndex, isRed,
               first = 'ZuzuAgent', second = 'ZuzuAgent'):
    """
    This function should return a list of two agents that will form the
    team, initialized using firstIndex and secondIndex as their agent
    index numbers.  isRed is True if the red team is being created, and
    will be False if the blue team is being created.

    As a potentially helpful development aid, this function can take
    additional string-valued keyword arguments ("first" and "second" are
    such arguments in the case of this function), which will come from
    the --redOpts and --blueOpts command-line arguments to capture.py.
    For the nightly contest, however, your team will be created without
    any extra arguments, so you should make sure that the default
    behavior is what you want for the nightly contest.
    """
    shared_dic = util.Counter()
    initSharedDic(shared_dic, firstIndex, secondIndex)

    return [eval(first)(firstIndex, shared_dic), eval(second)(secondIndex, shared_dic)]

def initSharedDic(shared_dic, firstIndex, secondIndex):
    shared_dic['max_root'] = util.Counter()
    shared_dic['max_root'][firstIndex] = util.Counter()
    shared_dic['max_root'][secondIndex] = util.Counter()
    shared_dic['max_offense'] = util.Counter()

    shared_dic['power_collector'] = util.Counter()

    shared_dic['weights'] = {}
    
    shared_dic['weights']['food_collector'] = {'path_safety': 3.620880583500029, 'food_eaten?': 0.0507055851756273, 'unsafe_tunnel?': -2.867899974, 'tagged?': -3.8921942980955117}
    
    shared_dic['weights']['power_collector'] = {'power_eaten?': 0.7112035549588391, 'path_safety': 3.655880583500029, 'unscared_ghosts': 1.1451387737, 'tagged?': -3.2921942980955117}
    
    shared_dic['weights']['returner'] = {'path_safety': 3.648880583500029, 'food_returned?': 3.8576220272128934, 'tagged?': -3.2921942980955117}
    
    shared_dic['weights']['tagger'] = {'tagged?': 2.8687861358586314, 'tag_distance': -4.5568707701494724, 'unsafe_tunnel?': 2.0876612974655785}
    
    shared_dic['weights']['anticipator'] = {'capsule_distance': -0.0111730941868121911, 'return_distance': -0.0018394538694459592, 'food_distance': -0.0071017189934083695}

    shared_dic['weights']['offense'] = {'child_score': 1., 'safe?': 1.} 
    
    shared_dic['weights']['defense'] = {'child_score': 1.} 
   
    shared_dic['weights']['root'] = {'child_score': 1.} 
    
    shared_dic['rewards'] = {}
    shared_dic['rewards']['food_collector'] = ZuzuAgent.computeFoodCollectorScore
    shared_dic['rewards']['power_collector'] = ZuzuAgent.computePowerCollectorScore
    shared_dic['rewards']['returner'] = ZuzuAgent.computeReturnerScore
    shared_dic['rewards']['offense'] = ZuzuAgent.computeOffenseScore
    shared_dic['rewards']['tagger'] = ZuzuAgent.computeTaggerScore
    shared_dic['rewards']['anticipator'] = ZuzuAgent.computeAnticipatorScore
    shared_dic['rewards']['defense'] = ZuzuAgent.computeAnticipatorScore
    shared_dic['rewards']['root'] = ZuzuAgent.computeAnticipatorScore

    #HMM inference shared information
    shared_dic['opponent1'] = None
    shared_dic['opponent2'] = None
    shared_dic['prob_enemy_pos'] = None 
    shared_dic['prev_prob_enemy_pos'] = None 

    return shared_dic

##########
# Agents #
##########

class ZuzuAgent(CaptureAgent):

    def __init__(self, index, shared_dic):
        CaptureAgent.__init__(self, index)
        
        self.shared_dic = shared_dic
    
    def registerInitialState(self, gameState):
        CaptureAgent.registerInitialState(self, gameState)
        self.start = gameState.data.agentStates[self.index].start.pos
        self.ally_start = gameState.data.agentStates[(self.index + 2) % 4].start.pos
        self.ally_index = (self.index + 2) % 4
        self.enemy_start = gameState.data.agentStates[( (self.index+1) % 2) + 2].start.pos
        self.starting_state = gameState.deepCopy()
        self.observationHistory = []
        
        self.root_agents = {}
        
        self.offensive_agents = {}
        
        self.agent_type = 'food_collector'
        self.food_collector_extractor = FoodCollectorExtractor(self)
        self.food_collector_qAgent = ApproximateQAgent(self, self.index, self.food_collector_extractor, self.shared_dic['weights'][self.agent_type], is_training)
        self.offensive_agents[self.agent_type] = self.food_collector_qAgent

        self.agent_type = 'power_collector'
        self.power_collector_extractor = PowerCollectorExtractor(self)
        self.power_collector_qAgent = ApproximateQAgent(self, self.index, self.power_collector_extractor, self.shared_dic['weights'][self.agent_type], is_training)
        self.offensive_agents[self.agent_type] = self.power_collector_qAgent
        
        self.agent_type = 'returner'
        self.returner_extractor = ReturnerExtractor(self)
        self.returner_qAgent = ApproximateQAgent(self, self.index, self.returner_extractor, self.shared_dic['weights'][self.agent_type], is_training)
        self.offensive_agents[self.agent_type] = self.returner_qAgent
        
        self.agent_type = 'offense'
        self.offense_extractor = OffenseExtractor(self, self.offensive_agents)
        self.offense_qAgent = ApproximateQAgent(self, self.index, self.offense_extractor, self.shared_dic['weights'][self.agent_type], is_training)
        self.root_agents[self.agent_type] = self.offense_qAgent
        
        self.defensive_agents = {}
        
        self.agent_type = 'tagger'
        self.tagger_extractor = TaggerExtractor(self)
        self.tagger_qAgent = ApproximateQAgent(self, self.index, self.tagger_extractor, self.shared_dic['weights'][self.agent_type], is_training)
        self.defensive_agents[self.agent_type] = self.tagger_qAgent

        self.agent_type = 'anticipator'
        self.anticipator_extractor = AnticipatorExtractor(self)
        self.anticipator_qAgent = ApproximateQAgent(self, self.index, self.anticipator_extractor, self.shared_dic['weights'][self.agent_type], is_training)
        self.defensive_agents[self.agent_type] = self.anticipator_qAgent
        
        self.agent_type = 'defense'
        self.defense_extractor = DefenseExtractor(self, self.defensive_agents)
        self.defense_qAgent = ApproximateQAgent(self, self.index, self.defense_extractor, self.shared_dic['weights'][self.agent_type], is_training)
        self.root_agents[self.agent_type] = self.defense_qAgent
        
        self.agent_type = 'root'
        self.root_extractor = RootExtractor(self, self.root_agents)
        self.root_qAgent = ApproximateQAgent(self, self.index, self.root_extractor, self.shared_dic['weights'][self.agent_type], is_training)
        
        self.qAgent = self.root_qAgent
        #self.qAgent = self.offense_qAgent
        #self.qAgent = self.food_collector_qAgent
        #self.qAgent = self.power_collector_qAgent
        #self.qAgent = self.returner_qAgent
        #self.qAgent = self.skedaddle_qAgent
        #self.qAgent = self.tagger_qAgent
        #self.qAgent = self.anticipator_qAgent
        #self.qAgent = self.defense_qAgent

        my_return_pts = getEdgePos(gameState, self.red)
        enemy_return_pts = getEdgePos(gameState, not self.red)
        self.edge_pts = [my_return_pts, enemy_return_pts] * 2
        if self.index % 2 == 1:
            self.edge_pts.reverse()
        
        self.prev_state = self.starting_state
        self.prev_action = None
        self.prev_score = 0

        self.enemy_scared = {i:0 for i in self.getOpponents(gameState)}
        
        self.num_carrying = 0

        self.num_returned = 0

        self.num_enemy_pacman = 0
        
        self.moves_remaining = 1200 / 4 - 1

        self.power_collector_score = 0.

        self.limit = 5

        self.shared_dic['prob_enemy_pos'] = self.approximateEnemyPositions(self.start, state=self.starting_state)
        self.shared_dic['prev_prob_enemy_pos'] = self.shared_dic['prob_enemy_pos']

        self.position_cache = set() 
        self.position_cache_list = []
        self.position_cache.add(self.start)
        self.position_cache_list.append(self.start)
        self.position_cache_size = 14
        self.repitition_limit = 6
    
    def chooseAction(self, gameState):
        agent_pos = gameState.data.agentStates[self.index].configuration.getPosition()
        
        self.shared_dic['prev_prob_enemy_pos'] = self.shared_dic['prob_enemy_pos']
        self.shared_dic['prob_enemy_pos'] = self.approximateEnemyPositions(agent_pos)
        #print "estimated pos: ", self.index, self.shared_dic['prob_enemy_pos']
        
        self.updatePositionCache(agent_pos)

        action_score, action = self.qAgent.getAction(gameState)
        
        if is_training:
            score = self.computeScore(gameState)
            
            if self.prev_state is not self.starting_state:
                self.qAgent.update(self.prev_state, self.prev_action, gameState, score)
            self.prev_score = score

        self.qAgent.featExtractor.process(self.prev_state, self.prev_action, gameState)
        self.prev_state = gameState.deepCopy()
        self.prev_action = action
        self.moves_remaining -= 1
        return action

    def computeScore(self, state):
        return self.shared_dic['rewards'][self.agent_type](self, state)
    
    # computes a score for the offensive parent agent
    def computeOffenseScore(self, state):
        return self.computeReturnerScore(state) + self.computeFoodCollectorScore(state)

    # The score for a food collector is
    # the difference between the amount he is carrying this iteration vs the previous
    def computeFoodCollectorScore(self, state):
        agent_state = state.data.agentStates[self.index]
        
        # check to see if we happen to have returned food
        # so that we do not return negative score
        if self.computeReturnerScore(state) > 0.:
            self.num_carrying = agent_state.numCarrying
            return 0.
        
        diff = agent_state.numCarrying - self.num_carrying
        self.num_carrying = agent_state.numCarrying
        return diff 

    # The score for the power collector is the number of ghosts
    # which change from not scared to scared
    def computePowerCollectorScore(self, state):
        score = 0
        agent_state = state.data.agentStates[self.index]
        pos = agent_state.getPosition()
        capsules = self.prev_state.getCapsules()
        enemy_idxs = self.getOpponents(state)
        for idx in enemy_idxs:
            enemy_agent = state.data.agentStates[idx]
            enemy_pos = enemy_agent.getPosition()
            if enemy_agent.isPacman:
                continue
            scared_timer = enemy_agent.scaredTimer
            prev_scared_timer = self.enemy_scared[idx]
            if agent_state.isPacman and pos in capsules:
                score += (scared_timer - prev_scared_timer) / 40.0
            self.enemy_scared[idx] = scared_timer
        return score

    # The score for the returner is the number
    # of food pellets that the agent returns
    def computeReturnerScore(self, state):
        agent_state = state.data.agentStates[self.index]
        num_returned = agent_state.numReturned
        diff = agent_state.numReturned - self.num_returned
        self.num_returned = agent_state.numReturned
        return diff

    # The score of the tagger is the number
    # of enemies that were just tagged
    def computeTaggerScore(self, state):
        score = 0
        enemy_tagged_count = 0
        prev_agent_pos = self.prev_state.data.agentStates[self.index].configuration.getPosition()
        agent_pos = state.data.agentStates[self.index].configuration.getPosition()
        enemy_idxs = self.getOpponents(state)
        for idx in enemy_idxs:
            enemy_state = state.data.agentStates[idx]
            prev_enemy_state = self.prev_state.data.agentStates[idx]
            enemy_pos = enemy_state.configuration.getPosition()
            if self.getMazeDistance(enemy_state.start.getPosition(), enemy_pos) <= 1:
                prev_enemy_pos = prev_enemy_state.configuration.getPosition() 
                if self.getMazeDistance(prev_enemy_pos, enemy_pos) > 1:
                    if self.getMazeDistance(prev_enemy_pos, agent_pos) <= 1:
                        enemy_tagged_count += 1
        score += enemy_tagged_count
        return score

    # The score of the anticipator decreases if the enemy
    # has just collected a power pellet or returned food
    def computeAnticipatorScore(self, state):
        score = 0.
        agent_state = state.data.agentStates[self.index]
        if agent_state.scaredTimer >= 40:
            score -= 1.
        curr_score = self.getScore(state)
        prev_score = self.getScore(self.prev_state)
        if curr_score < prev_score:
            score -= 1.
        return score

    # maintain a set of our most recent locations 
    # useful for avoiding repeating action loops
    def updatePositionCache(self, my_pos):
        

        #add new position
        self.position_cache_list.append(my_pos)
        self.position_cache.add(my_pos)

        #remove oldest element
        #starting position is at index 0, let's keep it that way
        if len(self.position_cache_list) > self.position_cache_size:
            oldest = self.position_cache_list[1]
            if oldest in self.position_cache:
                self.position_cache.remove(oldest)
            del self.position_cache_list[1]
    
    def final(self, state):
        #pass
        initSharedDic(self.shared_dic, self.index, self.ally_index) 
        self.qAgent.final(state)



    """
    HMM position approximization component
    """

    def approximateEnemyPositions(self, pos, state=None):
        prev_state = state
        if state is None:
            state = self.getCurrentObservation()
            prev_state = self.getPreviousObservation()
        enemy_idxs = self.getOpponents(state)
        self.shared_dic['opponent1'], enemy1 = self.approximateEnemyPosition(state, prev_state, pos, self.shared_dic['opponent1'], enemy_idxs[0])
        self.shared_dic['opponent2'], enemy2 = self.approximateEnemyPosition(state, prev_state, pos, self.shared_dic['opponent2'], enemy_idxs[1])
        #print [enemy1, enemy2]
        return [enemy1, enemy2]

    def approximateEnemyPosition(self, state, prev_state, pos, opp_dic, idx):
        walls = state.getWalls()

        #init the probability dictionaries
        if opp_dic is None or opp_dic == 0:
            return self.initOpponentDictionary(opp_dic, idx, state, walls)

        #check if we are close enough to read the actual position
        enemy_pos = state.data.agentStates[idx].getPosition()
        if enemy_pos is not None:
            enemy_pos = nearestPoint( enemy_pos )
            for key in opp_dic.keys():
                opp_dic[key] = 0.
            opp_dic[enemy_pos] = 1.0
            return opp_dic, enemy_pos
        
        food = self.getFoodYouAreDefending(state)
        prev_food = food
        if prev_state is not None:
            prev_food = self.getFoodYouAreDefending(prev_state)
        for x in range(walls.width):
            for y in range(walls.height):
                
                #if we see food dropped, we know the bot reset
                """
                if food[x][y] and not prev_food[x][y]:
                    i = int(idx / 2)
                    dist1 = self.getMazeDistance(pos, self.prob_enemy_pos[i])
                    dist2 = self.getMazeDistance(pos, self.prob_enemy_pos[(i+1)%2])
                    if dist1 <= dist2:
                        return self.initOpponentDictionary(opp_dic, idx, state, walls)
                """

                #if we see food eaten, we know a bot had to be there
                """
                if prev_food[x][y] and not food[x][y]:
                    i = int(idx / 2)
                    dist1 = self.getMazeDistance((x,y), self.prob_enemy_pos[i])
                    dist2 = self.getMazeDistance((x,y), self.prob_enemy_pos[(i+1)%2])
                    if dist1 <= dist2:
                        for key in opp_dic.keys():
                            opp_dic[key] = 0
                        opp_dic[(x,y)] = 1.0
                        return opp_dic, (x,y)
                """

        prev_probable_pos = opp_dic.argMax()
        prev_opp_dic = opp_dic.copy()

        noisy_ds = state.getAgentDistances()
        noisy_d = noisy_ds[idx]
        opp_dic_cp = opp_dic.copy()
        for key in opp_dic.keys():
            #if the position is close enough to have been detecable
            if util.manhattanDistance(pos, key) <= 5:
                opp_dic_cp[key] = 0.0
                continue
            dist = util.manhattanDistance(pos, key)
            #print "manh dist: ", dist
            dist_prob = state.getDistanceProb(dist, noisy_d)
            #print "dist prob: ", dist_prob
            neighbors = Actions.getLegalNeighbors(key, walls)
            sum_prob = 0.0
            #print neighbors
            for nei_pos in neighbors:
                nei_pos = nearestPoint(nei_pos)
                sum_prob += opp_dic[nei_pos]
            #print "neig prob: ", sum_prob
            opp_dic_cp[key] = dist_prob * sum_prob
        opp_dic = opp_dic_cp
        opp_dic.normalize()

        """
        print "non-zero-prob"
        for key in opp_dic.keys():
            if opp_dic[key] > 0.0:
                print key, opp_dic[key]
        """
        
        probable_pos = opp_dic.argMax()
        
        #if all p's are 0, then we lost sight
        #this happens when they get tagged
        if opp_dic[probable_pos] <= 0.0:
            #print "lost sight", prev_opp_dic.argMax()
            return self.initOpponentDictionary(opp_dic, idx, state, walls)

        return opp_dic, probable_pos

    def initOpponentDictionary(self, opp_dic, idx, state, walls):
            opp_dic = util.Counter()
            for x in range(walls.width):
                for y in range(walls.height):
                    if not walls[x][y]:
                       opp_dic[ (x,y) ] = 0.
            starting = state.getInitialAgentPosition(idx)
            #print "starting", idx, starting
            opp_dic[starting] = 1.
            return opp_dic, starting


def getEdgePos(state, is_red):
    walls = state.getWalls()
    pts = []
    side = 1
    if is_red:
        side = -1
    x = (state.data.layout.width + side) / 2
    for y in range(1, state.data.layout.height):
        if not walls[int(x)][int(y)]:
            pts.append( nearestPoint((x, y)) )
    return pts

def isOnOwnHalf(state, pos, is_red):
    x,y = pos
    side_rounding = 1.
    if is_red:
        side_rounding = -1.
    endX = state.data.layout.width/2.0 + side_rounding/2.0
    endX = int(endX)
    return (x - endX) * side_rounding >= 0
    

# returns the closest distance between the given position and list of positions
def closestDistance(agent, pos, coord_list):
    min_dist = agent.starting_state.data.layout.width * agent.starting_state.data.layout.height
    for pos2 in coord_list:
        dist = agent.getMazeDistance(pos, pos2)
        min_dist = min(min_dist, dist)
    return min_dist
    
# return the point in the list that is closest to the given position    
def closestDistancePosition(agent, pos, coord_list):
    min_dist = (999999, (-1, -1))
    for pos2 in coord_list:
        dist = (agent.getMazeDistance(pos, pos2), pos2)
        min_dist = min(min_dist, dist)
    return min_dist[1]
    
# returns the positions of the opposing agents
def getOpponentPositions(agent, state):
    #use position approximation if exists
    if agent.shared_dic['prob_enemy_pos'] is not None:
        return agent.shared_dic['prob_enemy_pos']
    enemy_pos = []
    enemy_idxs = agent.getOpponents(state)
    for idx in enemy_idxs:
        pos = state.data.agentStates[idx].getPosition()
        if pos is not None:
            enemy_pos.append( nearestPoint(pos) )
    return enemy_pos

# returns 2 lists of team ghost positions, indices
# these are allies which are on their own half,
# and are scared for less than the distance to the agent
def getDefenderPositions(agent, state, opp_pos):
    defenders_pos = []
    defenders_idx = []
    team_idx = agent.getTeam(state)
    for idx in team_idx:
        pos = state.data.agentStates[idx].getPosition()
        opp_dist = agent.getMazeDistance(opp_pos, pos)
        tag_dist = opp_dist
        if opp_dist < 3:
            tag_dist = opp_dist / 2.
        scared_timer = state.data.agentStates[idx].scaredTimer
        is_scared = scared_timer > tag_dist
        is_ghost = not state.data.agentStates[idx].isPacman
        if is_ghost and not is_scared:
            defenders_pos.append( nearestPoint(pos) )
            defenders_idx.append(idx)
    return tuple(defenders_pos), tuple(defenders_idx)
    

# returns 2 lists of opponent ghost positions, indices
# these are opponents which are on their own half,
# and are scared for less than double the distance to the agent
def getGhostPositions(agent, state, opponents_pos, my_pos):
    enemy_pos = []
    ghosts_idx = []
    enemy_idx = agent.getOpponents(state)
    for idx in enemy_idx:
        pos = opponents_pos[idx / 2]
        tag_dist = 2 * agent.getMazeDistance(my_pos, pos)
        scared_timer = state.data.agentStates[idx].scaredTimer
        is_scared = scared_timer > tag_dist
        is_ghost = not state.data.agentStates[idx].isPacman
        if is_ghost and not is_scared:
            enemy_pos.append( nearestPoint(pos) )
            ghosts_idx.append(idx)
    return tuple(enemy_pos), tuple(ghosts_idx)

# attempts to simulate the next move of the defensive opponents,
# and return its positions
#
# assume the ghosts will move towards the agent
def getNextGhostPositions(agent, state, opponents_idx, opponents_pos, pos):
    walls = state.getWalls()
    next_ghosts = []
    for idx in opponents_idx:
        enemy_pos = opponents_pos[0]
        if len(opponents_pos) > 1:
            enemy_pos = opponents_pos[idx / 2] 
        
        if pos == agent.start:
            next_ghosts.append( nearestPoint(enemy_pos) )
            continue
        x,y = enemy_pos
        min_dist = (999999, (x,y))
        #for action in state.getLegalActions(idx):
        for next_pos in Actions.getLegalNeighbors(enemy_pos, walls):
            #dx, dy = Actions.directionToVector(action)
            #next_x, next_y = nearestPoint((x + dx, y + dy))
            next_x, next_y = nearestPoint(next_pos)
            
            #ignore moves that cross halves
            if next_x == agent.edge_pts[agent.index][0][0]:
                continue
            
            dist = agent.getMazeDistance((next_x, next_y), pos)
            min_dist = min(min_dist, (dist, (next_x,next_y)))
        next_ghosts.append(min_dist[1])
    return tuple(next_ghosts)

# count the number of offensive opponent pacmen
def countEnemyPacman(agent, state):
    opp_idx = agent.getOpponents(state)
    pac_count = 0
    for idx in opp_idx:
        opp_state = state.data.agentStates[idx]
        if opp_state.isPacman:
            pac_count += 1
    return pac_count

class SimpleExtractor:
    """
    Returns simple features for a basic reflex Pacman:
    - whether food will be eaten
    - how far away the next food is
    - whether a ghost collision is imminent
    - whether a ghost is one step away
    """
    def __init__(self, agent):
        self.agent = agent
        self.defender = None
        self.offender = None
        self.recent_q = util.Queue()
    
    # called during the agent choose action method
    #
    # useful for updating feature behavior
    # based on actual moves made in the world
    def process(self, state, action, next_state):
        pass

    
    # takes a list of points on the grid,
    # and returns a grid with T for the given points
    def convertListToGrid(self, pt_list, width, height):
        grid = Grid(width, height)
        for pt in pt_list:
            grid[pt[0]][pt[1]] = True
        return grid

    def isOnOwnHalf(self, state, idx):
        return not state.getAgentState(idx).isPacman

    def closestFoodSearch(self, pos, food, walls):
        """
        closestFood -- this is similar to the function that we have
        worked on in the search project 
        """
        fringe = [(pos[0], pos[1], 0)]
        expanded = set()
        while fringe:
            pos_x, pos_y, dist = fringe.pop(0)
            if (pos_x, pos_y) in expanded:
                continue
            expanded.add((pos_x, pos_y))
            # if we find a food at this location then exit
            if food[pos_x][pos_y]:
                return dist
            # otherwise spread out from the location to its neighbours
            nbrs = Actions.getLegalNeighbors((pos_x, pos_y), walls)
            for nbr_x, nbr_y in nbrs:
                fringe.append((nbr_x, nbr_y, dist+1))
        # no food found
        return None

    # checks if the position is in the given list
    def checkPointInList(self, pos, pt_list):
        if pos in pt_list:
            return 1.
        return 0.
    
    # computes the distance proportion between the points in the given list and the given pos, opponent positions
    # returns the largest proportion to a point
    #
    # filters out points that ally can handle better
    def computeDistanceProportion(self, pt_list, pos, opponents_pos, ally_pos, width, height):
        if len(pt_list) == 0:
            return 0
        
        my_dist_to_ally = self.agent.getMazeDistance(pos, ally_pos)
        score = -1.
        for pt in pt_list:
            my_dist = self.agent.getMazeDistance(pt, pos)
            ally_dist = self.agent.getMazeDistance(pt, ally_pos)

            #our ally is directly along the path to the point
            #can get there sooner
            if my_dist == my_dist_to_ally + ally_dist:
                continue
            
            enemy_dist = self.agent.getMazeDistance(pt, self.agent.enemy_start) - 1
            for opp_pos in opponents_pos:
                dist = self.agent.getMazeDistance(pt, opp_pos)
                enemy_dist = min(enemy_dist, dist)
            proportion_score = (enemy_dist + 1.) / (my_dist + 1.) - 1.
            proportion_score /= (1.0 * width * height)
            score = max(score, proportion_score)
        return score

    # checks if it is possible to collect the nearest food and 
    # return to the tunnel entrance before a ghost arrives
    def checkForTunnel(self, walls, food, my_pos, opponents_states, opponents_pos, index, debug):
        tunnel_dist = 2 * closestDistance(self.agent, my_pos, food) + self.agent.getMazeDistance(my_pos, self.tunnel_pts[index])
        
        #filter out opponents that are scared for our distance to travel the tunnel
        new_opponents_pos = list(opponents_pos)
        new_opponents_states = list(opponents_states)
        for i in range(len(opponents_pos)):
            opp_pos = opponents_pos[i]
            opp_state = opponents_states[i]
            if self.agent.getMazeDistance(self.tunnel_pts[index], opp_pos) < opp_state.scaredTimer - 1:
                new_opponents_pos.remove(opp_pos)
                new_opponents_states.remove(opp_state)
        opponents_pos = new_opponents_pos
        opponents_states= new_opponents_states
        
        in_tunnel = self.insideTunnel(walls, my_pos, opponents_pos, index)

        if debug:
            print "tunnel pt", self.tunnel_pts[index]
            print "tunnel_dist", tunnel_dist
        
        # we are in a tunnel
        if in_tunnel:
            ghost_dist = closestDistance(self.agent, self.tunnel_pts[index], opponents_pos) - 1
            if debug:
                print "ghost_dist", ghost_dist
            # a ghost can reach the tunnel before we get out
            if ghost_dist < tunnel_dist:
                return 1.
        return 0.

    # checks to see if the agent is entering a tunnel, 
    # by checking if the only way out is by passing over the position we just came from
    #
    # returns True if we are in a tunnel
    def insideTunnel(self, walls, my_pos, opponents_pos, index):
        # make the position we were just in and opponent positions a wall, 
        # and see if we can return the food a different way
        mod_walls = walls.copy()
        new_opponents_pos = list(opponents_pos)

        #filter out opponents that are far away
        filter_dist = 9
        for opp_pos in opponents_pos:
            if self.agent.getMazeDistance(my_pos, opp_pos) >= filter_dist:
                new_opponents_pos.remove(opp_pos)
        opponents_pos = new_opponents_pos

        prev_x, prev_y = self.tunnel_pts[index]
        mod_walls[prev_x][prev_y] = True
        for opp_pos in opponents_pos:
            x, y = opp_pos
            mod_walls[x][y] = True
        
        return_grid = self.convertListToGrid(self.agent.edge_pts[index], walls.width, walls.height)

        nearest_food_dist = self.closestFoodSearch(my_pos, return_grid, mod_walls)
        in_tunnel = nearest_food_dist == None
        return in_tunnel
        
    def countUnscaredGhosts(self, state):
        scared_count = 0
        for idx in self.agent.getOpponents(state):
            enemy_agent = state.data.agentStates[idx]
            if enemy_agent.isPacman:
                continue
            scared_count += enemy_agent.scaredTimer
        return -scared_count / 80.0
    
    # return the cost of a safe path to the nearest possible given goal point
    #
    # if safe, returns a score from (0,1]
    # returns -1 if not safe
    def findSafestPathCost(self, state, my_pos, goal_pts, opponents_pos, ally_pos, width, height, debug):
        max_goal_nodes = 5
        expand_limit = self.agent.limit
        if self.agent.getScore(state) > 0:
            max_goal_nodes -= 2
        goal_dists = util.PriorityQueue()
        count = 0

        for pt in goal_pts:
            dist_score = self.computeDistanceProportion([pt], my_pos, opponents_pos, ally_pos, width, height) 
            goal_dists.push(pt, -dist_score)
            count += 1
        for i in range(0, min(count, max_goal_nodes)):
            goal_pt = goal_dists.pop()
            #try the absolute nearest position on the last iteration
            if i == max_goal_nodes - 1 and max_goal_nodes > 1:
                goal_pt = closestDistancePosition(self.agent, my_pos, goal_pts)
            limit = max(expand_limit - i, 2)
            safe_path_cost = self.findSafePath(state, my_pos, goal_pt, limit)
            if safe_path_cost < width * height:
                if debug:
                    print "safest goal from-to:", my_pos, goal_pt
                self.path_cost = safe_path_cost
                return 1. * ((width * height) - safe_path_cost) / (width * height) 
        self.path_cost = -1
        return -1.

    # returns a cost associated with the safety and length of the calculated path 
    # between the given start, goal points
    def findSafePath(self, state, my_pos, goal_pos, limit):
        self.agent.problem = SafeTravelProblem(state, my_pos, [goal_pos], self.agent, safetyHeuristic, limit)
        path, cost = aStarSearch(self.agent, safetyHeuristic, expand_limit=390)
        return cost
    
    # computes the distance between the given agent position and opponent position
    #
    # handles edge cases related to tagging and scared timers
    def getTagDistance(self, my_pos, agent_state, opp_pos, opponent_state, opponent_start, walls, debug):
        dist = self.agent.getMazeDistance(my_pos, opp_pos)
        if opp_pos == opponent_start:
            dist = 0.
        if agent_state.scaredTimer > dist:
            dist = walls.width * walls.height
        if  agent_state.isPacman and (opponent_state.scaredTimer <= dist or dist > 1):
            dist = walls.width * walls.height

        return dist

class SimpleParentExtractor(SimpleExtractor):
    
    def __init__(self, agent, child_agents={}):
        SimpleExtractor.__init__(self, agent)
        self.child_agents = child_agents
    
    # call process on all children agents
    def process(self, state, action, next_state):
        for qAgent in self.child_agents.values():
            qAgent.featExtractor.process(state, action, next_state)

    def checkForRedundantTask(self, features_val, my_pos, ally_pos, walls):
        #when both agents have the same task
        #discourage moving towards eachother
        moves_remaining = self.agent.moves_remaining
        if len(self.agent.shared_dic['max_offense'].keys()) > 0:
            last_stored = min(self.agent.shared_dic['max_offense'].keys())
            if last_stored - moves_remaining <= 1:
                moves_remaining = last_stored 
        my_max_type = features_val.argMax()
        if my_max_type == 'returner':
            return
        ally_root_type = self.agent.shared_dic['max_root'][self.agent.ally_index][moves_remaining]
        if ally_root_type != 0:
            ally_root_type = ally_root_type[1]
        if ally_root_type == 'offense':
            value = self.agent.shared_dic['max_offense'][moves_remaining]
            if value == 0:
                self.agent.shared_dic['max_offense'][moves_remaining] = (features_val[my_max_type], my_max_type)
                return
            ally_score, ally_type = value 
            if my_max_type == ally_type:
                if features_val[my_max_type] > ally_score:
                    self.agent.shared_dic['max_offense'][self.agent.moves_remaining] = (features_val[my_max_type], my_max_type)
                elif self.agent.getMazeDistance(my_pos, ally_pos) < 2 and self.agent.moves_remaining < 260:
                    ally_separation_score = 2. * abs( ( (walls.width * walls.height) - math.pow(self.agent.getMazeDistance(my_pos, ally_pos), 3) ) ) / (walls.width * walls.height)
                    features_val[my_max_type] -= ally_separation_score


class RootExtractor(SimpleParentExtractor):

    def getFeatures(self, state, action, debug=False):
        walls = state.getWalls()
        next_state = state.generateSuccessor(self.agent.index, action)
        width = state.data.layout.width
        height = state.data.layout.height
        prev_agent_state = state.data.agentStates[self.agent.index]
        prev_pos = nearestPoint(prev_agent_state.getPosition())
        agent_state = next_state.data.agentStates[self.agent.index]
        my_pos = nearestPoint(agent_state.getPosition())
        opponents_pos = getOpponentPositions(self.agent, next_state)
        opponents_idx = self.agent.getOpponents(next_state)
        ally_agent_state = state.data.agentStates[self.agent.ally_index]
        ally_pos = ally_agent_state.getPosition()
        food = self.agent.getFood(state)
        score = self.agent.getScore(next_state)

        features = util.Counter()

        #avoid both agents sitting in the same cell
        if my_pos == prev_pos and my_pos == ally_pos:
            features['child_score'] = -6.
            return features

        #if we're losing, refraine from stopping 2x in a row
        if action == Directions.STOP and self.agent.prev_action == action and score <= 0:
            features['child_score'] = -6.
            return features

        #if we're in a loop sequence,
        #and we aren't winning,
        #discourage a move to a position in the loop
        opp_neighbors = []
        for opp_pos in self.agent.shared_dic['prob_enemy_pos']:
            opp_neighbors.append( Actions.getLegalNeighbors(opp_pos, walls) )
        if score <= 0 and len(self.agent.position_cache) <= self.agent.repitition_limit and len(self.agent.position_cache_list) >= self.agent.position_cache_size:
            if my_pos in self.agent.position_cache or (my_pos in opp_neighbors and agent_state.isPacman):
                #print "avoiding repeating sequence", my_pos, self.agent.position_cache
                features['child_score'] = -1.9
                return features

        #if we're waiting along the half, stray away from ally
        ally_dist = closestDistance(self.agent, ally_pos, (prev_pos, my_pos))
        if score <= 0 and ally_pos[0] == prev_pos[0] == my_pos[0] == self.agent.edge_pts[self.agent.index][0][0] and ally_dist == self.agent.getMazeDistance(my_pos, ally_pos):
            features['child_score'] = -6.
            return features
             
        #if we're scared, don't get tagged 
        if not agent_state.isPacman and agent_state.scaredTimer > 0 and closestDistance(self.agent, my_pos, opponents_pos) <= 1:
            features['child_score'] = -6.
            return features

        
        features_val = util.Counter()
        for agent_type in self.agent.root_agents:
            qAgent = self.agent.root_agents[agent_type]
            features_val[agent_type] = qAgent.getQValue(state.deepCopy(), action)

        max_value = self.agent.shared_dic['max_root'][self.agent.index][min(self.agent.shared_dic['max_root'][self.agent.index].keys()+[300])]
        if max_value == 0:
            max_value = (0, 0)
        max_type = max_value[0]

        #if we were on the defensive half 
        #and are not scared to tag a pacman
        #and we are winning
        #encourage defense
        if (not prev_agent_state.isPacman or max_type == 'defense') and score > 0 and agent_state.scaredTimer - 1 < closestDistance(self.agent, my_pos, opponents_pos) and ally_agent_state.isPacman:
            if action != Directions.STOP or self.agent.prev_action != action:
                features_val['offense'] -= 5.

        #if the enemy is very far away, encourage offense
        dist_to_food = 2 * closestDistance(self.agent, my_pos, food.asList())
        dist_to_enemy = closestDistance(self.agent, my_pos, opponents_pos)
        if dist_to_enemy > dist_to_food and countEnemyPacman(self.agent, next_state) == 0:
            features_val['offense'] += 5.
        
        max_type = features_val.argMax()
        max_value = self.agent.shared_dic['max_root'][self.agent.index][self.agent.moves_remaining]
        if max_value == 0:
            max_value = (0, 0)
        self.agent.shared_dic['max_root'][self.agent.index][self.agent.moves_remaining] = max(max_value, (features_val[max_type], max_type) )
       
        
        ally_value = self.agent.shared_dic['max_root'][self.agent.ally_index][min(self.agent.shared_dic['max_root'][self.agent.ally_index].keys()+[300])]
        if ally_value != 0:
            ally_max_value, ally_type = ally_value
            my_dist = closestDistance(self.agent, my_pos, opponents_pos)
            ally_dist = closestDistance(self.agent, ally_pos, opponents_pos)
            if score <= 0 and ally_type == 'defense' and ally_dist < my_dist:
                features_val['offense'] += 2.

        features['child_score'] = features_val[max_type]
        
        return features

class DefenseExtractor(SimpleParentExtractor):

    def getFeatures(self, state, action, debug=False):
        walls = state.getWalls()
        next_state = state.generateSuccessor(self.agent.index, action)
        width = state.data.layout.width
        height = state.data.layout.height
        prev_agent_state = state.data.agentStates[self.agent.index]
        prev_pos = nearestPoint(prev_agent_state.getPosition())
        agent_state = next_state.data.agentStates[self.agent.index]
        my_pos = nearestPoint(agent_state.getPosition())
        
        features = util.Counter()

        features_val = util.Counter()
        for agent_type in self.agent.defensive_agents:
            qAgent = self.agent.defensive_agents[agent_type]
            features_val[agent_type] = qAgent.getQValue(state.deepCopy(), action)

        features['child_score'] = features_val['anticipator']
        if features_val['anticipator'] > 0 or self.agent.moves_remaining < 300:
            features['child_score'] = sum(features_val.values())
        
        return features
    
class OffenseExtractor(SimpleParentExtractor):

    def getFeatures(self, state, action, debug=False):
        walls = state.getWalls()
        next_state = state.generateSuccessor(self.agent.index, action)
        prev_agent_state = state.data.agentStates[self.agent.index]
        prev_pos = nearestPoint(prev_agent_state.getPosition())
        agent_state = next_state.data.agentStates[self.agent.index]
        my_pos = nearestPoint(agent_state.getPosition())
        prev_num_carrying = prev_agent_state.numCarrying
        num_carrying = agent_state.numCarrying
        num_returned = agent_state.numReturned
        ally_agent_state = state.data.agentStates[self.agent.ally_index]
        ally_pos = ally_agent_state.getPosition()
        food = self.agent.getFood(state)
                
        features = util.Counter()

        features_val = util.Counter()
        for agent_type in self.agent.offensive_agents:
            qAgent = self.agent.offensive_agents[agent_type]
            features_val[agent_type] = qAgent.getQValue(state.deepCopy(), action)
            #print agent_type, features_val[agent_type]
        
        #do not consider the power collector score
        #if we cannot return in time
        if self.agent.returner_extractor.path_cost != -1 and self.agent.returner_extractor.path_cost + 2 > self.agent.moves_remaining:
            features_val['power_collecter'] = 0.
            features_val['returner'] = 0.

        features['safe?'] = 0.
        #print "power_collector:", features_val['power_collector']
        if features_val['power_collector'] > 0. or features_val['returner'] > 0.:
            features['safe?'] = 1.

        #do not consider the returner score
        #if we aren't carrying anything
        if num_carrying + prev_num_carrying == 0 or my_pos == self.agent.start:
            features_val['returner'] = 0.
       
        #we only need to return all but 2 to win
        if num_carrying > 0 and len(food.asList()) <= 2:
            features_val['food_collector'] = 0.
        
        # adjust values based on communication from our ally bot
        
        #let the ally closer to the capsule be the power collector
        max_power_score = self.agent.shared_dic['power_collector'][self.agent.moves_remaining]
        if features_val['power_collector'] > max_power_score:
            self.agent.shared_dic['power_collector'][self.agent.moves_remaining] = features_val['power_collector']
        else:
            features_val['power_collector'] = 0.
        

        self.checkForRedundantTask(features_val, my_pos, ally_pos, walls)
        
        features['child_score'] = features_val[features_val.argMax()]
        
        #print "from-to:", prev_pos, my_pos
        #print "returner:", features_val['returner']
        #print "food_col:", features_val['food_collector']
        #print "capsule: ", features_val['power_collector']


        return features

class AnticipatorExtractor(SimpleExtractor):

    def getFeatures(self, state, action, debug=False):
        walls = state.getWalls()
        next_state = state.generateSuccessor(self.agent.index, action)
        width = state.data.layout.width
        height = state.data.layout.height
        prev_agent_state = state.data.agentStates[self.agent.index]
        prev_pos = nearestPoint(prev_agent_state.getPosition())
        agent_state = next_state.data.agentStates[self.agent.index]
        my_pos = nearestPoint(agent_state.getPosition())
        ally_agent_state = state.data.agentStates[(self.agent.index + 2) % 4]
        ally_pos = ally_agent_state.getPosition()
        team_states = (agent_state, ally_agent_state)
        food = self.agent.getFoodYouAreDefending(state)
        capsules = self.agent.getCapsulesYouAreDefending(state)
        opponents_idx = self.agent.getOpponents(next_state)
        opponents_pos = getOpponentPositions(self.agent, next_state)
        opponent_return_pts = self.agent.edge_pts[self.agent.index]

        features = util.Counter()

        capsule_distances = []
        return_distances = []
        food_distances = []

        for idx in range(len(opponents_idx)):
        
            opp_idx = opponents_idx[idx]
            opp_pos = opponents_pos[idx / 2]
            opponent_start = state.getInitialAgentPosition(idx)
            defenders_pos, defenders_idx = getDefenderPositions(self.agent, next_state, opp_pos)
           
            scored_defensive_pos = [my_pos]
            capsule_distance = self.computeDistanceProportion(capsules, opp_pos, scored_defensive_pos, opponent_start, width, height) 
            capsule_distances.append( (closestDistance(self.agent, opp_pos, capsules), capsule_distance) )
        
            return_distance = self.computeDistanceProportion(opponent_return_pts, opp_pos, scored_defensive_pos, opponent_start, width, height) 
            return_distances.append(return_distance)

            food_distance = self.computeDistanceProportion(food.asList(), opp_pos, scored_defensive_pos, opponent_start, width, height) 
            food_distances.append(return_distance)
       
        if len(capsule_distances) > 0:
            features['capsule_distance'] = min(capsule_distances)[1]
        if features['capsule_distance'] > 0.:
            features['capsule_distance'] = 1.
        
        features['return_distance'] = max(return_distances)
        if features['return_distance'] > 0.:
            features['return_distance'] = 1.
        
        features['food_distance'] = max(food_distances)
        if features['food_distance'] > 0.:
            features['food_distance'] = 1.
        
        return features

class TaggerExtractor(SimpleExtractor):

    def getFeatures(self, state, action, debug=False):
        walls = state.getWalls()
        next_state = state.generateSuccessor(self.agent.index, action)
        width = state.data.layout.width
        height = state.data.layout.height
        prev_agent_state = state.data.agentStates[self.agent.index]
        prev_pos = nearestPoint(prev_agent_state.getPosition())
        agent_state = next_state.data.agentStates[self.agent.index]
        my_pos = nearestPoint(agent_state.getPosition())
        ally_agent_state = state.data.agentStates[(self.agent.index + 2) % 4]
        ally_pos = ally_agent_state.getPosition()
        team_states = (agent_state, ally_agent_state)
        food = self.agent.getFoodYouAreDefending(state)
        opponents_idx = self.agent.getOpponents(next_state)
        
        features = util.Counter()
        if state == self.agent.starting_state:
            return features

        my_ally_dist = self.agent.getMazeDistance(my_pos, ally_pos)
        dists = []
        for idx in range(len(opponents_idx)):
        
            opp_idx = opponents_idx[idx]
            opponent_state = next_state.data.agentStates[opp_idx]
            #opp_pos = nearestPoint( opponent_state.getPosition() )
            opp_pos = nearestPoint( self.agent.shared_dic['prob_enemy_pos'][idx] )
            prev_opp_pos = nearestPoint( self.agent.shared_dic['prev_prob_enemy_pos'][idx] )
            opponent_start = opponent_state.start.getPosition()
            defenders_pos, defenders_idx = getDefenderPositions(self.agent, next_state, opp_pos)

            features['unsafe_tunnel?'] += self.checkForTunnel(walls, food.asList(), opp_pos, team_states, defenders_pos, opp_idx, debug=debug)

            #if self.agent.getMazeDistance(prev_opp_pos, opp_pos) > 1:
            features['tagged?'] += self.checkPointInList(opp_pos, (opponent_start, my_pos))

            dist = self.getTagDistance(my_pos, agent_state, opp_pos, opponent_state, opponent_start, walls, debug=debug)
            
            #do not move to tag along the same path as an ally
            my_dist = self.agent.getMazeDistance(my_pos, opp_pos)
            ally_dist = self.agent.getMazeDistance(ally_pos, opp_pos)
            if my_ally_dist + ally_dist != my_dist or closestDistance(self.agent, ally_pos, getOpponentPositions(self.agent, next_state)) != ally_dist:
                dists.append(dist)
            if idx == opponents_idx[-1] and len(dists) == 0:
                dists.append(dist)

        
        features['tag_distance'] = 1. * min(dists + [walls.width * walls.height]) / (walls.width * walls.height) - 1.

        if debug:
            print features
        
        return features

    def process(self, state, action, next_state):
        SimpleExtractor.process(self, state, action, next_state)

        prev_pos = state.getAgentPosition(self.agent.index)
        my_pos = next_state.getAgentPosition(self.agent.index)
        self.processTunnel(next_state, prev_pos, my_pos)
    
    # keeps track of the tunnel entrance
    #
    # once we detect we are in a tunnel,
    # do not update the tunnel entrance point 
    # until we are no longer in a tunnel
    def processTunnel(self, state, prev_pos, my_pos):
        for idx in self.agent.getOpponents(state):
            opp_pos = nearestPoint( self.agent.shared_dic['prob_enemy_pos'][idx/2] )
            #opp_pos = nearestPoint( state.data.agentStates[idx].getPosition() )
            defenders_pos, defenders_idx = getDefenderPositions(self.agent, state, opp_pos)
            inside_tunnel = self.insideTunnel(state.getWalls(), opp_pos, defenders_pos, idx)

            if not inside_tunnel:
                self.tunnel_pts[idx] = opp_pos 

    def __init__(self, agent):
        SimpleExtractor.__init__(self, agent)
        self.tunnel_pts = util.Counter()
        for idx in agent.getOpponents(agent.starting_state):
            opp_pos = agent.starting_state.data.agentStates[idx].getPosition()
            self.tunnel_pts[idx] = opp_pos



class FoodCollectorExtractor(SimpleExtractor):

    def getFeatures(self, state, action, debug=False):
        walls = state.getWalls()
        next_state = state.generateSuccessor(self.agent.index, action)
        width = state.data.layout.width
        height = state.data.layout.height
        prev_agent_state = state.data.agentStates[self.agent.index]
        prev_pos = nearestPoint(prev_agent_state.getPosition())
        agent_state = next_state.data.agentStates[self.agent.index]
        my_pos = nearestPoint(agent_state.getPosition())
        ally_agent_state = state.data.agentStates[(self.agent.index + 2) % 4]
        ally_pos = ally_agent_state.getPosition()
        food = self.agent.getFood(state)
        opponents_pos = getOpponentPositions(self.agent, state)
        opponents_states = [state.data.agentStates[(self.agent.index + 1) % 4], state.data.agentStates[(self.agent.index + 3) % 4]]
        next_opponents_pos = getNextGhostPositions(self.agent, next_state, self.agent.getOpponents(next_state), self.agent.shared_dic['prob_enemy_pos'], my_pos)
        
        ghosts_pos, ghosts_idx = getGhostPositions(self.agent, next_state, self.agent.shared_dic['prob_enemy_pos'], my_pos)
        next_ghosts_pos = getNextGhostPositions(self.agent, next_state, ghosts_idx, self.agent.shared_dic['prob_enemy_pos'], my_pos)
       
        features = util.Counter()
        if len(food.asList()) == 0:
            return features

        features['path_safety'] = self.findSafestPathCost(next_state, my_pos, food.asList(), next_ghosts_pos, ally_pos, width, height, debug)

        features['food_eaten?'] = self.checkPointInList(my_pos, food.asList())
        
        features['tagged?'] = self.checkPointInList(my_pos, (self.agent.start, self.agent.ally_start)+next_ghosts_pos)

        features['unsafe_tunnel?'] = self.checkForTunnel(walls, food.asList(), my_pos, opponents_states, next_opponents_pos, self.agent.index, debug=debug)

        return features
    
    def process(self, state, action, next_state):
        SimpleExtractor.process(self, state, action, next_state)

        prev_pos = state.getAgentPosition(self.agent.index)
        my_pos = next_state.getAgentPosition(self.agent.index)
        self.processTunnel(next_state, prev_pos, my_pos)
    
    # keeps track of the tunnel entrance
    #
    # once we detect we are in a tunnel,
    # do not update the tunnel entrance point 
    # until we are no longer in a tunnel
    def processTunnel(self, state, prev_pos, my_pos):
        #print "tunnel pt", self.tunnel_pt
        ghosts_pos, ghosts_idx = getGhostPositions(self.agent, state, self.agent.shared_dic['prob_enemy_pos'], my_pos)
        inside_tunnel = self.insideTunnel(state.getWalls(), my_pos, ghosts_pos, self.agent.index)

        if not inside_tunnel:
            self.tunnel_pts[self.agent.index] = my_pos 

    def __init__(self, agent):
        SimpleExtractor.__init__(self, agent)
        self.tunnel_pts = util.Counter()
        self.tunnel_pts[agent.index] = self.agent.start

class PowerCollectorExtractor(SimpleExtractor):

    def getFeatures(self, state, action, debug=False):
        #walls = state.getWalls()
        width = state.data.layout.width
        height = state.data.layout.height
        capsules = self.agent.getCapsules(state)
        next_state = state.generateSuccessor(self.agent.index, action)
        prev_agent_state = state.data.agentStates[self.agent.index]
        prev_pos = nearestPoint(prev_agent_state.getPosition())
        agent_state = next_state.data.agentStates[self.agent.index]
        my_pos = nearestPoint(agent_state.getPosition())
        ally_agent_state = state.data.agentStates[(self.agent.index + 2) % 4]
        ally_pos = ally_agent_state.getPosition()
        ghosts_pos, ghosts_idx = getGhostPositions(self.agent, next_state, self.agent.shared_dic['prob_enemy_pos'], my_pos)
        next_ghosts_pos = getNextGhostPositions(self.agent, next_state, ghosts_idx, self.agent.shared_dic['prob_enemy_pos'], my_pos)
        
        features = util.Counter()
        if len(capsules) == 0:
            return features

        features['path_safety'] = self.findSafestPathCost(next_state, my_pos, capsules, next_ghosts_pos, ally_pos, width, height, debug)

        features['power_eaten?'] = self.checkPointInList(my_pos, capsules)
        
        features['tagged?'] = self.checkPointInList(my_pos, (self.agent.start,)+next_ghosts_pos)

        features['unscared_ghosts'] = self.countUnscaredGhosts(state)    

        return features
       

class ReturnerExtractor(SimpleExtractor):

    def getFeatures(self, state, action, debug=False):
        next_state = state.generateSuccessor(self.agent.index, action)
        width = state.data.layout.width
        height = state.data.layout.height
        prev_agent_state = state.data.agentStates[self.agent.index]
        prev_pos = nearestPoint(prev_agent_state.getPosition())
        agent_state = next_state.data.agentStates[self.agent.index]
        my_pos = nearestPoint(agent_state.getPosition())
        ghosts_pos, ghosts_idx = getGhostPositions(self.agent, next_state, self.agent.shared_dic['prob_enemy_pos'], my_pos)
        next_ghosts_pos = getNextGhostPositions(self.agent, next_state, ghosts_idx, self.agent.shared_dic['prob_enemy_pos'], my_pos)
        return_pts = self.agent.edge_pts[self.agent.index]
        prev_num_carrying = prev_agent_state.numCarrying
        num_carrying = agent_state.numCarrying

        features = util.Counter()
       
        features['path_safety'] = self.findSafestPathCost(next_state, my_pos, return_pts, next_ghosts_pos, self.agent.start, width, height, debug)
        
        features['food_returned?'] = self.checkPointInList(my_pos, return_pts)

        features['tagged?'] = self.checkPointInList(my_pos, (self.agent.start,)+next_ghosts_pos)

        return features

class QLearningAgent:
    def __init__(self, index, is_training, alpha=.1, epsilon=0.10, gamma=.99, numTraining = 10):
        self.discount = float(gamma)
        self.numTraining = int(numTraining)
        if not is_training:
            self.alpha = 0.0
            self.epsilon = 0.0
        else:
            self.alpha = float(alpha)
            self.epsilon = float(epsilon)
        self.index = index

        self.values = util.Counter() # A Counter is a dict with default 0

        self.num_iterations = 0

    def computeValueFromQValues(self, state):
        """
          Returns max_action Q(state,action)
          where the max is over legal actions.  Note that if
          there are no legal actions, which is the case at the
          terminal state, you should return a value of 0.0.
        """
        return self.computeFromQValues(state)[0]       

    def computeActionFromQValues(self, state):
        """
          Compute the best action to take in a state.  Note that if there
          are no legal actions, which is the case at the terminal state,
          you should return a random one.
        """
        max_action = self.computeFromQValues(state)
        action = random.choice(max_action[1])
        return (max_action[0], action)

    def computeFromQValues(self, state):
        legalActions = state.getLegalActions(self.index)
        max_action = (0, ['eRror'])
        if len(legalActions) > 0:
            max_action = (-999999, [random.choice(legalActions)])
        for action in legalActions:
            val = self.getQValue(state, action)
            if max_action[0] == val:
                max_action[1].append(action)
            else:
                max_action = max(max_action, (val, [action]))
        return max_action

    def getAction(self, state):
        """
          Compute the action to take in the current state.  With
          probability self.epsilon, we should take a random action and
          take the best policy action otherwise.  Note that if there are
          no legal actions, which is the case at the terminal state, you
          should choose random as the action.

            returns tuple of score, action
        """
        # Pick Action
        legalActions = state.getLegalActions(self.index)
        action = (0, random.choice(legalActions) )
        if util.flipCoin(self.epsilon):
            return action

        return self.getPolicy(state)


    def getPolicy(self, state):
        return self.computeActionFromQValues(state)

    def getValue(self, state):
        return self.computeValueFromQValues(state)


class PacmanQAgent(QLearningAgent):
    "Exactly the same as QLearningAgent, but with different default parameters"

    def __init__(self, index, is_training):
        """
        These default parameters can be changed from the pacman.py command line.
        For example, to change the exploration rate, try:
            python pacman.py -p PacmanQLearningAgent -a epsilon=0.1

        alpha    - learning rate
        epsilon  - exploration rate
        gamma    - discount factor
        numTraining - number of training episodes, i.e. no learning after these many episodes
        """
        self.index = index
        QLearningAgent.__init__(self, index, is_training)

    def getAction(self, state):
        """
        Simply calls the getAction method of QLearningAgent and then
        informs parent of action for Pacman.  Do not change or remove this
        method.
        """
        action = QLearningAgent.getAction(self,state)
        #self.doAction(state,action)
        return action


class ApproximateQAgent(PacmanQAgent):
    def __init__(self, agent, index, featExtractor, weights={}, is_training=True):
        self.featExtractor = featExtractor 
        self.agent = agent

        """
        is_training = False
        if len(weights) == 0:
            weights = util.Counter()
            is_training = True
        """
        self.weights = weights

        self.weight_counts = util.Counter()

        PacmanQAgent.__init__(self, index, is_training)

    def getWeights(self):
        return self.weights

    def getQValue(self, state, action):
        """
          Should return Q(state,action) = w * featureVector
          where * is the dotProduct operator
        """
        features = self.featExtractor.getFeatures(state,action)
        q_sum = 0
        for key in features:
            q_sum += features[key] * self.weights[key]
        #print "q sum:",q_sum
        return q_sum

    def update(self, state, action, nextState, reward):
        """
           Should update your weights based on transition
        """
        debug=False
        if reward != 0:
            print "reward, from-to:", reward, state.getAgentPosition(self.agent.index), nextState.getAgentPosition(self.agent.index)
            debug=True
        features = self.featExtractor.getFeatures(state, action, debug=debug)
        #if features['tagged?'] == 1. and features['unsafe_tunnel?'] < 1:
        #    print "tagged but not unsafe tunnel"
        #if reward < 0 and features['tagged?'] < 1:
        #    print "Got tagged, but tag feature not labeled!\n"
        #    features = self.featExtractor.getFeatures(state, action, debug=True)
        #if features['unsafe_tunnel?'] == 1.:
        #    print "We are in an unsafe tunnel", state.getAgentPosition(self.agent.index), nextState.getAgentPosition(self.agent.index), self.featExtractor.tunnel_pt
        #    features = self.featExtractor.getFeatures(state, action, debug=True)
        next_state_q = self.computeValueFromQValues(nextState)
        state_q = self.getQValue(state, action)
        for key in features:
            alpha = self.alpha 
            diff = (reward + self.discount * next_state_q) - state_q
            #if reward != 0:
            #    print "key, feature, weight:", key, features[key], self.weights[key]
            self.weights[key] += alpha*diff*features[key]
            #if reward != 0:
            #    print "new weight:", self.weights[key]


        #self.num_iterations += 1
        #self.alpha = 1. / math.sqrt(self.num_iterations)

    def final(self, state):
        "Called at the end of each game."
        # call the super-class final method
        #PacmanQAgent.final(self, state)

        # did we finish training?
        #if self.episodesSoFar == self.numTraining:
            # you might want to print your weights here for debugging
        #print self.index, self.weights
        pass

class aStarStrategy:
    def __init__(self, agent, heuristic):
        self.agent = agent
        self.heuristic = heuristic
    
    def strategy(self, node):
        return node[2] + self.heuristic(node[0], self.agent)

def aStarSearch(agent, heuristic, debug=False, timer=None, expand_limit=None):
    """Search the node that has the lowest combined cost and heuristic first."""
    a_star = aStarStrategy(agent, heuristic)
    fringe = util.PriorityQueueWithFunction(a_star.strategy)
    return search(agent.problem, fringe, debug, timer, expand_limit)

def search(problem, fringe, debug=False, timer=None, expand_limit=None):
    visited = set()
    fringe.push([problem.getStartState(), [], problem.getStartCost()])
    #print "starting search!"
    while not fringe.isEmpty():
        #time limit reached, need to exit
        #if timer is not None and time.time() - timer > 14.2:
        #    #print "time threshold exceeded"
        #    return None
        node = fringe.pop()
        state = node[0]
        path = node[1]
        cost = node[2]
        #reached expansion limit
        if expand_limit is not None and expand_limit < problem.getNumberExpanded():
            lay = problem.startingGameState.data.layout
            return [], lay.width * lay.height 
        if problem.isGoalState(state, cost):
            #if debug:
            #    print "solution:"
            #    print "state:", state
            #    print "cost:", cost
            #    print "path:", path
            return path, cost
        if state not in visited:
            #if debug:
            #    print "state:", state
            #    print "cost:", cost
            visited.add(state)
            for child_node in problem.getSuccessors(state):
                new_path = list(path)
                new_path.append(child_node[1])
                new_cost = cost + child_node[2]
                fringe.push((child_node[0], new_path, new_cost))
    return [], -1

# sample heuristic
def noneHeuristic(state, agent):
    return agent.problem.getStartCost()

# returns a score of 0 if a ghost cannot interfere with our current path
# else returns a score > 0
def safetyHeuristic(search_state, agent):
    #print "search state ghost pos:", search_state[2][0]
    score = 0
    game_state = agent.problem.startingGameState
    
    my_pos = search_state[0]
    if isOnOwnHalf(game_state, my_pos, agent.red):
        return 0

    ghosts_pos = search_state[2][0]
    for ghost_pos in ghosts_pos:
        ghost_dist = agent.getMazeDistance(ghost_pos, my_pos)
        if ghost_dist <= 1:
            score += game_state.data.layout.width * game_state.data.layout.height

    return score

class Problem(object):

    def __init__(self, startingGameState, start_pos, food, idx, is_red, minCount=0):
        self.start = (start_pos, tuple(food))
        self.walls = startingGameState.getWalls()
        self.startingGameState = startingGameState
        self._expanded = 0
        self.xDiff = 1 #-1 for red, 1 for blue
        if is_red:
            self.xDiff = -1
        self.minCount = minCount # min number of food remaining required by default for the goal state
        
        self.start_pos = start_pos
        self.food = food
        self.idx = idx
        self.is_red = is_red

    def copy(self):
        new_problem = Problem(self.startingGameState, self.start_pos, self.food, self.idx, self.is_red, self.minCount)
        return new_problem

    def getExpandedCount(self):
        return self._expanded

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        pos = self.start[0]
        food = list(self.start[1])
        #handle if we are starting on a food
        if pos in food:
            food.remove(pos)
            start = list(self.start)
            start[1] = tuple(food)
            self.start = tuple(start)
        return self.start

    def getStartCost(self):
        """
        Used to define different types of 0s to initiate starting cost
        """
        return 0

    def isGoalState(self, state, cost):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        if len(state[1]) <= self.minCount:
            return True
        return False
    
 
    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        successors = []
        self._expanded += 1 
        for direction in [Directions.NORTH, Directions.SOUTH, Directions.EAST, Directions.WEST]:
            x,y = state[0]
            dx, dy = Actions.directionToVector(direction)
            nextx, nexty = int(x + dx), int(y + dy)
            if not self.walls[nextx][nexty]:
                nextFood = list(state[1])
                if (nextx, nexty) in nextFood:
                    nextFood.remove( (nextx, nexty) )
                successors.append( ( ((nextx, nexty), tuple(nextFood)), direction, 1) )
        return successors

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        return len(actions)

    def getNumberExpanded(self):
        return self._expanded

class SafeTravelProblem(Problem):

    def __init__(self, startingGameState, start_pos, goal_pos, agent, cost_heuristic, move_limit):
        Problem.__init__(self, startingGameState, start_pos, goal_pos, agent.index, agent.red)

        ghosts_pos, ghosts_idx = getGhostPositions(agent, startingGameState, agent.shared_dic['prob_enemy_pos'], start_pos)
        #need to let the ghosts move first
        next_ghosts_pos = getNextGhostPositions(agent, startingGameState, ghosts_idx, ghosts_pos, start_pos)
        #ghosts_pos = (((1, 3), (1, 1)), (0,2)) 
        self.start = (start_pos, tuple(goal_pos), (next_ghosts_pos, ghosts_idx) )
        #print "start:", self.start
        #print agent.shared_dic['prob_enemy_pos'] 
        self.agent = agent
        self.cost_heuristic = cost_heuristic
        self.move_limit = move_limit
    
    # modify the original successor function by adding predicted ghost positions to the state
    def getSuccessors(self, state):
        successors = Problem.getSuccessors(self, state)
        new_successors = []
        for child in successors:
            child_state = child[0]
            my_pos = child_state[0]
            ghosts_pos, ghosts_idx = state[2]
            next_ghosts_pos = ghosts_pos
            if self.move_limit > self.getNumberExpanded():
                next_ghosts_pos = getNextGhostPositions(self.agent, self.startingGameState, ghosts_idx, ghosts_pos, my_pos)
            #print self.agent.limit, self.getNumberExpanded()
            #print "playe_pos:", my_pos
            #print "ghost_pos:", ghosts_pos
            #print "nextg_pos:", next_ghosts_pos
             
            ghost_info = (next_ghosts_pos, ghosts_idx)
            new_state = (child_state[0], child_state[1], ghost_info)
            new_cost = child[2] + self.cost_heuristic(new_state, self.agent)
            new_successors.append( ( new_state, child[1], new_cost) )
        return new_successors

    def isGoalState(self, state, cost):
        if cost > (self.startingGameState.data.layout.width * self.startingGameState.data.layout.height):
            return True
        return Problem.isGoalState(self, state, cost)
