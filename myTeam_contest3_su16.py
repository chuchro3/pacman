"""
Agent from summer 2016, using basic q-learning to train an offensive and defensive agent 
w/ hmm inference


Implemented by: Robert Chuchro
"""

# myTeam.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from captureAgents import CaptureAgent
import random, time, util
from game import Directions, Actions, Configuration
import game
from util import nearestPoint

#################
# Team creation #
#################

def createTeam(firstIndex, secondIndex, isRed,
               first = 'ZuzuAgent', second = 'ZuzuAgent'):
  """
  This function should return a list of two agents that will form the
  team, initialized using firstIndex and secondIndex as their agent
  index numbers.  isRed is True if the red team is being created, and
  will be False if the blue team is being created.

  As a potentially helpful development aid, this function can take
  additional string-valued keyword arguments ("first" and "second" are
  such arguments in the case of this function), which will come from
  the --redOpts and --blueOpts command-line arguments to capture.py.
  For the nightly contest, however, your team will be created without
  any extra arguments, so you should make sure that the default
  behavior is what you want for the nightly contest.
  """

  # The following line is an example only; feel free to change it.
  return [eval(first)(firstIndex), eval(second)(secondIndex)]

##########
# Agents #
##########

class ZuzuAgent(CaptureAgent):

    def __init__(self, index):
        CaptureAgent.__init__(self, index)
        
 
        
        
        #defensive_weights = {'#-of-pacman-1-step-away': 373.73548658032746, 'further-from-capsule': -418.0534026884393, 'not-on-defense': -32.84700910597272, 'anticipation-dist': -14.639432813935732, 'closest-pacman': -36.4627155610598, 'num-attackers': -844.4072481441988}        
        offensive_weights = {}
        defensive_weights = {}

        offensive_weights = {'reset': -470000.142854895292515, 'closest-safe-capsule': -45.8976489118802, 'on-offense': 249.88921553709974, 'eats-capsule': 39.5977066533466577, '#-of-ghosts-1-step-away': -5780.53177479845513, 'closest-safe-food': -20.125069578668274, 'returns-food': 233.9665965427378, 'eats-food': 29.58885261402763, 'stop': -100.0, 'repetitive-move': -20.0}
        defensive_weights = {'enemy-within-1': 10.4071468551509447, 'further-from-capsule': -224.20300578971128, 'not-on-defense': -30.102432989207248, 'num-attackers': -34.6588811706434, 'closest-enemy': -8.6902725376992904, 'ally-dist': 1.0}
        #offensive_weights = {'enemy-within-1': 20.4071468551509447, 'further-from-capsule': -20.20300578971128, 'not-on-defense': -30.102432989207248, 'num-attackers': -44.6588811706434, 'closest-enemy': -16.6902725376992904, 'ally-dist': 16.65}

        extractor = OffenseExtractor(self)
        weights = offensive_weights
        #extractor = DefenseExtractor(self)
        #weights = defensive_weights
        if self.index > 1:
            extractor = DefenseExtractor(self)
            weights = defensive_weights

        self.extractor = extractor
        self.offense_extractor = OffenseExtractor(self)

        #is_training = True 
        is_training = False 
        
        self.qAgent = ApproximateQAgent(self, self.index, self.extractor, weights, is_training)
        self.defense_qAgent = self.qAgent
        self.offense_qAgent = ApproximateQAgent(self, self.index, self.offense_extractor, offensive_weights, is_training)
    
    def registerInitialState(self, gameState):
        CaptureAgent.registerInitialState(self, gameState)
        self.extractor.__init__(self)
        self.observationHistory = []

        self.start = gameState.getAgentPosition(self.index)
        self.enemy_start = gameState.getAgentPosition((self.index+1)%4)
        self.edge_pts = self.getEdgePos(gameState)

        self.prev_state = None
        self.prev_action = None
        self.prev_score = 0
        self.returned_count = 0
        self.num_carrying = 0
        self.capsules = 2
        self.carry_amount = 6
        if self.index > 1:
            self.carry_amount = 2
        self.opponent1 = None
        self.opponent2 = None
        self.prob_enemy_pos = []

    def chooseAction(self, gameState):
        score = self.computeScore(gameState)
        if self.prev_state is not None:
            score_diff = score
            self.qAgent.update(self.prev_state, self.prev_action, gameState, score_diff)
        if self.index > 1:
            if self.switchToOffense(gameState):
                self.qAgent = self.offense_qAgent
            else:
                self.qAgent = self.defense_qAgent
        action = self.qAgent.getAction(gameState)
        self.prev_state = gameState.deepCopy()
        self.prev_action = action
        self.prev_score = score
        return action

    def computeScore(self, state):
        self.prob_enemy_pos = self.approximateEnemyPositions(state.getAgentPosition(self.index))
        if self.index < 2 or self.switchToOffense(state):
            return self.computeOffenseScore(state)
        return self.computeDefenseScore(state)
    
    def switchToOffense(self, state):
        score = self.getScore(state)
        num_carrying = state.data.agentStates[self.index].numCarrying
        if score < 0:
            return True
        if state.data.agentStates[self.index].scaredTimer > 0:
            return True
        if num_carrying > 0:
            return True
        if not self.extractor.isOnOwnHalf(state, self.index):
            return True

        return False


    def computeOffenseScore(self, state):
        #score = self.getScore(state)
        score = 0
        num_returned = state.data.agentStates[self.index].numReturned
        num_carrying = state.data.agentStates[self.index].numCarrying
        pos = state.getAgentPosition(self.index)
        if abs(pos[0] - state.data.layout.width / 2.0) >= 1:
            score -= 1.0 if self.extractor.isOnOwnHalf(state, self.index) and num_carrying == 0 else 0.0
        
        returned_diff = num_returned - self.returned_count
        carrying_diff = num_carrying - self.num_carrying
        if carrying_diff > 0:
            score += carrying_diff
            self.num_carrying = num_carrying
        elif carrying_diff < 0 and returned_diff <= 0:
            score -= 100
            self.carry_amount = max(self.carry_amount-2, 0)
            self.num_carrying = 0
        else:
            self.num_carrying = 0
        #print "num_carrying: ", num_carrying
        if returned_diff > 0:
            score += returned_diff * 20.0
            #print "num_returned: ", returned_diff * 20.0
            self.returned_count = num_returned

        if pos == self.start:
            score -= 1.0
            #print "at start: -1"

        enemy_idx = self.getOpponents(state)
        for idx in enemy_idx:
            if state.data.agentStates[idx].scaredTimer == 40:
                score += 4
                #print "scaredTimer: 4"
        return score
   
    def computeDefenseScore(self, state):
        score = 0
        my_pos = state.getAgentPosition(self.index)
        
        score -= 0.0 if self.extractor.isOnOwnHalf(state, self.index) else 1.0

        if state.data.agentStates[self.index].scaredTimer == 40:
            score -= 40
        
        enemy_idx = self.getOpponents(state)
        num_returned = 0
        for idx in enemy_idx:
            score -= state.data.agentStates[idx].numCarrying
        
        returned_diff = num_returned - self.returned_count
        if returned_diff > 0:
            score -= returned_diff * 20.0
            #print "num_returned: ", returned_diff * 20.0
            self.returned_count = num_returned
        return score
    
    def final(self, state):
        self.qAgent.final(state)

    def getEdgePos(self, state):
        walls = state.getWalls()
        pts = []
        side = 1.0
        if self.red:
            side = -1
        x = state.data.layout.width / 2.0 + side
        for y in range(1, state.data.layout.height):
            if not walls[int(x)][int(y)]:
                pts.append( nearestPoint((x, y)) )
        return pts

    def approximateEnemyPositions(self, pos):
        state = self.getCurrentObservation()
        enemy_idxs = self.getOpponents(state)
        self.opponent1, enemy1 = self.approximateEnemyPosition(pos, self.opponent1, enemy_idxs[0])
        self.opponent2, enemy2 = self.approximateEnemyPosition(pos, self.opponent2, enemy_idxs[1])
        return [enemy1, enemy2]
        #return [(5,5), enemy2]

    def approximateEnemyPosition(self, pos, opp_dic, idx):
        state = self.getCurrentObservation()
        prev_state = self.getPreviousObservation()
        walls = state.getWalls()

        #init the probability dictionaries
        if opp_dic is None:
            return self.initOpponentDictionary(opp_dic, idx, state, walls)

        #check if we are close enough to read the actual position
        enemy_pos = state.data.agentStates[idx].getPosition()
        if enemy_pos is not None:
            for key in opp_dic.keys():
                opp_dic[key] = 0
            opp_dic[enemy_pos] = 1.0
            return opp_dic, enemy_pos
        
        food = self.getFoodYouAreDefending(state)
        prev_food = food
        if prev_state is not None:
            prev_food = self.getFoodYouAreDefending(prev_state)
        for x in range(walls.width):
            for y in range(walls.height):
                
                #if we see food dropped, we know the bot reset
                if food[x][y] and not prev_food[x][y]:
                    i = int(idx / 2)
                    dist1 = self.getMazeDistance(pos, self.prob_enemy_pos[i])
                    dist2 = self.getMazeDistance(pos, self.prob_enemy_pos[(i+1)%2])
                    if dist1 <= dist2:
                        return self.initOpponentDictionary(opp_dic, idx, state, walls)
                
                #if we see food eaten, we know a bot had to be there
                if prev_food[x][y] and not food[x][y]:
                    i = int(idx / 2)
                    dist1 = self.getMazeDistance((x,y), self.prob_enemy_pos[i])
                    dist2 = self.getMazeDistance((x,y), self.prob_enemy_pos[(i+1)%2])
                    if dist1 <= dist2:
                        for key in opp_dic.keys():
                            opp_dic[key] = 0
                        opp_dic[(x,y)] = 1.0
                        return opp_dic, (x,y)

        prev_probable_pos = opp_dic.argMax()
        prev_opp_dic = opp_dic.copy()

        noisy_ds = state.getAgentDistances()
        noisy_d = noisy_ds[idx]
        opp_dic_cp = opp_dic.copy()
        for key in opp_dic.keys():
            #if the position is close enough to have been detecable
            if util.manhattanDistance(pos, key) <= 5:
                opp_dic_cp[key] = 0.0
                continue
            #print key, pos, noisy_d
            #dist = self.getMazeDistance(pos, key)
            #print "maze dist: ", dist
            dist = util.manhattanDistance(pos, key)
            #print "manh dist: ", dist
            dist_prob = state.getDistanceProb(dist, noisy_d)
            #print "dist prob: ", dist_prob
            neighbors = Actions.getLegalNeighbors(key, walls)
            sum_prob = 0.0
            #print neighbors
            for nei_pos in neighbors:
                sum_prob += opp_dic[nei_pos]
            #print "neig prob: ", sum_prob
            opp_dic_cp[key] = dist_prob * sum_prob
        opp_dic = opp_dic_cp
        opp_dic.normalize()

        """
        print "non-zero-prob"
        for key in opp_dic.keys():
            if opp_dic[key] > 0.0:
                print key, opp_dic[key]
        """
         
        probable_pos = opp_dic.argMax()
        #if all p's are 0, then we lost sight
        if opp_dic[probable_pos] <= 0.0:

            opp_dic_cp = opp_dic.copy()
            for key in opp_dic.keys():
                #if the position is close enough to have been detecable
                if util.manhattanDistance(pos, key) <= 5:
                    opp_dic_cp[key] = 0.0
                    continue
                dist = util.manhattanDistance(pos, key)
                dist_prob = state.getDistanceProb(dist, noisy_d)
                opp_dic_cp[key] = dist_prob
            opp_dic = opp_dic_cp
            opp_dic.normalize()

            """
            print "non-zero-prev-prob"
            for key in prev_opp_dic.keys():
                if prev_opp_dic[key] > 0.0:
                    print key, prev_opp_dic[key]
            print "prev pos: ", prev_probable_pos, prev_state.getAgentDistances()[idx], prev_opp_dic[prev_probable_pos]
            print "prob pos: ", probable_pos, state.getAgentDistances()[idx], opp_dic[probable_pos]
            print noisy_ds[5] #EXCEPTION
            """
        return opp_dic, probable_pos

    def initOpponentDictionary(self, opp_dic, idx, state, walls):
            opp_dic = util.Counter()
            for x in range(walls.width):
                for y in range(walls.height):
                    if not walls[x][y]:
                       opp_dic[ (x,y) ] = 0.
            starting = state.getInitialAgentPosition(idx)
            opp_dic[starting] = 1.
            return opp_dic, starting

class SimpleExtractor:
    """
    Returns simple features for a basic reflex Pacman:
    - whether food will be eaten
    - how far away the next food is
    - whether a ghost collision is imminent
    - whether a ghost is one step away
    """
    def __init__(self, agent):
        self.agent = agent
        self.defender = None
        self.offender = None
        self.recent_q = util.Queue()

    def getGhostPositions(self, state):
        enemy_pos = [] 
        enemy_idx = self.agent.getOpponents(state)
        for idx in enemy_idx:
            pos = state.data.agentStates[idx].getPosition()
            is_scared = state.data.agentStates[idx].scaredTimer > 0
            if self.isOnOwnHalf(state, idx): #and not is_scared:
                enemy_pos.append(pos)
        return enemy_pos

    def getPacmanPositions(self, state):
        enemy_pos = []
        pacman_idxs = []
        enemy_idx = self.agent.getOpponents(state)
        for idx in enemy_idx:
            pos = state.data.agentStates[idx].getPosition()
            if not self.isOnOwnHalf(state, idx):
                enemy_pos.append(pos)
                pacman_idxs.append(idx)
        return enemy_pos, pacman_idxs
    
    def isOnOwnHalf(self, state, idx):
        return not state.getAgentState(idx).isPacman

    def closestFoodSearch(self, pos, food, walls):
        """
        closestFood -- this is similar to the function that we have
        worked on in the search project; here its all in one place
        """
        fringe = [(pos[0], pos[1], 0)]
        expanded = set()
        while fringe:
            pos_x, pos_y, dist = fringe.pop(0)
            if (pos_x, pos_y) in expanded:
                continue
            expanded.add((pos_x, pos_y))
            # if we find a food at this location then exit
            #if food[pos_x][pos_y]:
            if (pos_x, pos_y) in food:
                return dist
            # otherwise spread out from the location to its neighbours
            nbrs = Actions.getLegalNeighbors((pos_x, pos_y), walls)
            for nbr_x, nbr_y in nbrs:
                fringe.append((nbr_x, nbr_y, dist+1))
        # no food found
        return None

    def closestSafeReturn(self, pos, ghosts, walls, offense, num_carrying):
        if not offense or num_carrying == 0:
            return 999999
        coord_list = self.agent.edge_pts
        if not offense:
            return len(coord_list)
        min_dist = 999999
        for g_pt in ghosts:
            g_x, g_y = nearestPoint(g_pt)
            walls[g_x][g_y] = True
        for i in range(len(coord_list)):
            dist = self.closestFoodSearch(pos, coord_list, walls)
            if dist is not None:
                min_dist = min(min_dist, dist)
        return min_dist 

    def numSafeExits(self, pos, ghosts, walls, offense):
        coord_list = self.agent.edge_pts
        if not offense:
            return len(coord_list)
        num_safe = 0
        for i in range(len(coord_list)):
            pos2 = self.closestPosCoord(pos, coord_list)
            dist = self.agent.getMazeDistance(pos, pos2)
            ghost_dist = self.closestGhostPos(ghosts, [pos2])
            if dist < ghost_dist-1: 
                num_safe += 1
            else:
                for g_pt in ghosts:
                    g_x, g_y = nearestPoint(g_pt)
                    walls[g_x][g_y] = True
                alternate = self.closestFoodSearch(pos, coord_list, walls)
                if alternate is not None:
                    num_safe += 1
        return num_safe

    def closestSafePos(self, pos, coord_list, ghosts, walls, offense, default_val=999999, num_carrying=0):
        #if num_carrying > 0 and len(ghosts) > 0:
            #coord_list = self.agent.edge_pts
        if num_carrying > 0:
            coord_list += self.agent.edge_pts
        if num_carrying > self.agent.carry_amount:
            coord_list = list(self.agent.edge_pts)
        for g_pt in ghosts:
            g_x, g_y = nearestPoint(g_pt)
            walls[g_x][g_y] = True
        
        min_secondary = default_val
        alternate = self.closestFoodSearch(pos, coord_list, walls)
        if alternate is not None:
            min_secondary = alternate
        
        for i in range(len(coord_list)):
            pos2 = self.closestPosCoord(pos, coord_list)
            dist = self.agent.getMazeDistance(pos, pos2)
            ghost_dist = self.closestGhostPos(ghosts, [pos2])
            if dist < ghost_dist: 
                return dist
            #if ghost_dist > 5 and not offense:
               # min_secondary = dist
            coord_list.remove(pos2)
        #return self.agent.getMazeDistance(pos, self.agent.start)
        return min_secondary

    def closestPosCoord(self, pos, coord_list):
        min_dist = (999999, (-1, -1))
        for pos2 in coord_list:
            dist = (self.agent.getMazeDistance(pos, pos2), pos2)
            min_dist = min(min_dist, dist)
        return min_dist[1]
    
    def closestPos(self, pos, coord_list):
        min_dist = 999999
        for pos2 in coord_list:
            dist = self.agent.getMazeDistance(pos, pos2)
            min_dist = min(min_dist, dist)
        return min_dist

    def closestGhostPos(self, ghosts, coord_list):
        min_dist = 999999
        for pos in ghosts:
            dist = self.closestPos(pos, coord_list)
            min_dist = min(min_dist, dist)
        return min_dist

    def getClosestGhost(self, pos, ghosts):
        if len(ghosts) == 0:
            return [self.agent.start]
        min_dist = (999999, (-1,-1))
        for g_pos in ghosts:
            dist = self.agent.getMazeDistance(pos, g_pos)
            if dist < min_dist[0]:
                min_dist = (dist, g_pos)
            return [min_dist[1]]

    def countPellets(self, state):
        count = 0 
        enemy_idx = self.agent.getOpponents(state)
        for idx in enemy_idx:
            count += state.data.agentStates[idx].numCarrying
        return count

    def getNextGhostPositions(self, state, ghosts, pos):
        next_ghosts = []
        idx_list = []
        for idx in self.agent.getOpponents(state):
            ghost_pos = state.data.agentStates[idx].getPosition()
            if ghost_pos is None or ghost_pos not in ghosts:
                continue
            x,y = ghost_pos
            min_dist = (999999, (x,y))
            for action in state.getLegalActions(idx):
                dx, dy = Actions.directionToVector(action)
                next_x, next_y = nearestPoint((x + dx, y + dy))
                dist = self.agent.getMazeDistance((next_x, next_y), pos)
                min_dist = min(min_dist, (dist, (next_x,next_y)))
            next_ghosts.append(min_dist[1])
            idx_list.append(idx)
        return next_ghosts, idx_list


    def computeClosestNoisyDistance(self, state):
        noisy_ds = state.getAgentDistances()
        enemy_idxs = self.agent.getOpponents(state)
        min_dist = 999999
        for idx in enemy_idxs:
            dist = noisy_ds[idx]
            min_dist = min(min_dist, dist)
        return min_dist

    def getEnemyPosition(self, state):
        enemy_pos = []
        enemy_idxs = self.agent.getOpponents(state)
        for idx in enemy_idxs:
            pos = state.data.agentStates[idx].getPosition()
            if pos is not None:
                enemy_pos.append(pos)
        return enemy_pos

    def getClosestDistance(self, pos, other_pos):
        min_dist = 999999
        for pos2 in other_pos:
            dist = self.agent.getMazeDistance(pos, pos2)
            min_dist = min(min_dist, dist)
        return min_dist
        
    def predictNextDefensiveMove(self, next_state, enemy_pos, capsules, food, walls, pos):
        next_enemy_pos = []
        coord_list = capsules
        if len(capsules) == 0 or self.agent.index > 1:
            coord_list = food.asList()
        enemy_configs = []
        for e_pos in enemy_pos:
            enemy_configs.append(Configuration(e_pos, Directions.STOP))
        for e_config in enemy_configs:
            min_dist = (999999, e_config.pos)
            x,y = e_config.pos
            for action in Actions.getPossibleActions(e_config, walls):
                dx, dy = Actions.directionToVector(action)
                next_x, next_y = nearestPoint((x + dx, y + dy))
                closest_pos = self.getClosestGhost((x,y), coord_list)[0]
                if (next_x, next_y) == pos:
                    continue
                dist = self.agent.getMazeDistance((next_x, next_y), closest_pos)
                min_dist = min(min_dist, (dist, (next_x, next_y)))
            next_enemy_pos.append(min_dist[1])
        return next_enemy_pos

    def filterForGhosts(self, state, enemy_idxs, enemy_pos):
        new_enemys = [] 
        for i in range(len(enemy_idxs)):
            e_idx = enemy_idxs[i]
            pos = enemy_pos[i]
            if not self.isOnOwnHalf(state, e_idx):
                new_enemys.append(pos)
        if len(new_enemys) > 0:
            return new_enemys
        return enemy_pos

    def filterForPacman(self, state, enemy_idxs, enemy_pos):
        new_enemys = [] 
        for i in range(len(enemy_idxs)):
            e_idx = enemy_idxs[i]
            pos = enemy_pos[i]
            if self.isOnOwnHalf(state, e_idx):
                new_enemys.append(pos)
        if len(new_enemys) > 0:
            return new_enemys
        return enemy_pos

class OffenseExtractor(SimpleExtractor):
    
    def getFeatures(self, state, action):
        # extract the grid of food and wall locations and get the ghost locations
        nextState = state.generateSuccessor(self.agent.index, action)
        food = self.agent.getFood(state)
        capsules = self.agent.getCapsules(state)
        walls = state.getWalls().copy()
        ghosts = self.getGhostPositions(state)
        filter(lambda x: x != None, ghosts)
        agentState = state.data.agentStates[self.agent.index]
        my_pos = agentState.getPosition()
        next_agentState = nextState.data.agentStates[self.agent.index]
        next_pos = nextState.data.agentStates[self.agent.index].getPosition()
        next_ghosts, ghost_idxs = self.getNextGhostPositions(state, ghosts, my_pos)
        #print "ghost: ", ghosts
        #print "next : ", next_ghosts
        ghosts = next_ghosts
        num_returned = nextState.data.agentStates[self.agent.index].numReturned
        prev_num_returned = state.data.agentStates[self.agent.index].numReturned
        num_carrying = nextState.data.agentStates[self.agent.index].numCarrying
        prev_num_carrying = state.data.agentStates[self.agent.index].numCarrying
        diff_carrying = num_carrying - prev_num_carrying 
        diff_returned = num_returned - prev_num_returned 

        features = util.Counter()
         
        # compute the location of pacman after he takes the action
        x, y = agentState.getPosition()
        dx, dy = Actions.directionToVector(action)
        next_x, next_y = int(x + dx), int(y + dy)

        
        enemy_idxs = self.agent.getOpponents(state)
        ghosts_copy = list(ghosts)
        idx_copy = list(enemy_idxs)
        for i in range(len(ghosts_copy)):
            idx = idx_copy[i]
            ghost_pos = ghosts_copy[i]
            timer = state.data.agentStates[idx].scaredTimer
            dist = self.agent.getMazeDistance((next_x, next_y), ghost_pos)
            if dist < timer:
                    ghosts.remove(ghost_pos)
                    enemy_idxs.remove(idx)

        """
        for idx in list(ghost_idxs):
            timer = state.data.agentStates[idx].scaredTimer
            ghost_pos = state.data.agentStates[idx].getPosition()
            dist = self.agent.getMazeDistance((next_x, next_y), ghost_pos)
            if dist < timer:
                if ghost_pos in ghosts:
                    del ghosts[idx]
                    ghost_idxs.remove(idx)
        """


        ghosts = self.getClosestGhost((next_x, next_y), ghosts)
        if len(ghosts) == 0:
            capsules = []
        
        # count the number of ghosts 1-step away
        #features["#-of-ghosts-1-step-away"] = sum((next_x, next_y) in Actions.getLegalNeighbors(g, walls) for g in ghosts)
        features["#-of-ghosts-1-step-away"] = 0
        for ghost_pos in ghosts:
            dist = self.agent.getMazeDistance((next_x, next_y), ghost_pos)
            if dist <= 2:
                features["#-of-ghosts-1-step-away"] += 1
        # if there is no danger of ghosts then add the food feature
        if not features["#-of-ghosts-1-step-away"] and food[next_x][next_y]:
            features["eats-food"] = 1.0
        if not features["#-of-ghosts-1-step-away"] and (next_x, next_y) in capsules:
            features["eats-capsule"] = 1.0

        if not next_agentState.isPacman and diff_returned > 0:
            features["returns-food"] = 1.0

        offense = False
        if self.isOnOwnHalf(nextState, self.agent.index) and agentState.numCarrying == 0 and abs(next_pos[0] - state.data.layout.width / 2.0) >= 1:
            features['on-offense'] = 0.0 
        else:
            features['on-offense'] = 1.0 
            offense = True

        if next_pos == self.agent.start:
            features['reset'] = 1.0


        ally_idx = (self.agent.index +2) % 4
        ally_pos = state.data.agentStates[ally_idx].getPosition()
        for ghost_pos in ghosts:
            dist = self.agent.getMazeDistance(ally_pos, ghost_pos)
            if dist <= 3:
                ghosts.remove(ghost_pos)
                x,y = nearestPoint(ghost_pos)
                walls[x][y] = True

        #default_dist = walls.width * walls.height
        default_dist = 999999
        capsule_dist = self.closestSafePos((next_x, next_y), capsules, ghosts, walls.copy(), offense, default_dist)
        if capsule_dist != 999999:
            features["closest-safe-capsule"] = float(capsule_dist) / (walls.width * walls.height)
            #print "capsule", capsule_dist 
            #features["closest-safe-capsule"] = 1 / (float(capsule_dist) + .1)
        
        food_list = food.asList()
        #dist = self.closestPos((next_x, next_y), food_list)
        dist = self.closestSafePos((next_x, next_y), food_list, ghosts, walls.copy(), offense, default_dist, prev_num_carrying)
        if dist != 999999: #and not features["closest-safe-capsule"]:
            # make the distance a number less than one otherwise the update
            # will diverge wildly
            features["closest-safe-food"] = float(dist) / (walls.width * walls.height)
            #print "food ", dist 
            #features["closest-safe-food"] = 1 / (float(dist) + .1)
        """
        if not features["closest-safe-capsule"]:
            num_safe_exits = self.numSafeExits((next_x, next_y), ghosts, walls.copy(), offense)
            features["no-safe-exit"] =  1.0 if num_safe_exits == 0 else 0.0
        """
        """
        self.recent_q.push( (x,y) )
        if len(self.recent_q.list) > 100:
            self.recent_q.pop()
        count = 0
        for q_pos in self.recent_q.list:
            if next_pos == q_pos:
                count += 1
        if count > 20:
            features['repetitive-move'] = 1.0
            print 'repitition: ', next_pos
        """

        #return_dist = self.closestSafeReturn((next_x, next_y), ghosts, walls.copy(), offense, num_carrying)
        #if return_dist != 999999 and not features["closest-safe-capsule"]:
            #features["return-dist"] = float(return_dist) / (walls.width * walls.height)
            #features["close-return-dist"] = 1.0 if return_dist < 5 else 0.0
        
        #rev = Directions.REVERSE[agentState.configuration.direction]
        #if action == rev: features['reverse'] = 1
        if action == Directions.STOP: features['stop'] = 1.0

        return features

class DefenseExtractor(SimpleExtractor):
    
    def getFeatures(self, state, action):
        pos = state.data.agentStates[self.agent.index].getPosition()
        next_state = state.generateSuccessor(self.agent.index, action) 
        next_pos = next_state.data.agentStates[self.agent.index].getPosition()
        food = self.agent.getFoodYouAreDefending(state)
        capsules = self.agent.getCapsulesYouAreDefending(state)
        walls = state.getWalls()
        x, y = state.data.agentStates[self.agent.index].getPosition()

        features = util.Counter()

        enemy_idxs = self.agent.getOpponents(state)
        enemy_pos = self.agent.prob_enemy_pos
        enemy_pos = self.filterForGhosts(state, enemy_idxs, enemy_pos)
        enemy_pos = self.predictNextDefensiveMove(next_state, enemy_pos, capsules, food, walls, (x,y))

        #features["#-of-pacman-1-step-away"] = sum(next_pos in Actions.getLegalNeighbors(g, walls) for g in enemy_pos)
        
        closest_enemy_dist = self.getClosestDistance(next_pos, enemy_pos)
        if closest_enemy_dist < 1.0:
            features["enemy-within-1"] = 1.0


        features["closest-enemy"] = float(closest_enemy_dist) / (walls.width * walls.height)
        features['not-on-defense'] = 0.0 if self.isOnOwnHalf(next_state, self.agent.index) else 1.0

        # compute the location of pacman after he takes the action
        dx, dy = Actions.directionToVector(action)
        next_x, next_y = int(x + dx), int(y + dy)

        # count the number of enemy pacman 1-step away
        

        noisy_dist = self.computeClosestNoisyDistance(next_state)
        #if noisy_dist != 999999:
            #features["closest-noisy-dist"] = float(noisy_dist) / (walls.width * walls.height)

        features['num-attackers'] = 0.0
        for e_idx in enemy_idxs:
            if not self.isOnOwnHalf(state, e_idx):
                features['num-attackers'] += 1

        
        
        capsule_dist = self.closestPos((next_x, next_y), capsules)
        closest_capsule = None
        for ghost in enemy_pos:
            closest_capsule = self.getClosestGhost(ghost, capsules)
            if closest_capsule is not None:
                pacman_capsule_dist = self.closestGhostPos(enemy_pos, closest_capsule)
                if pacman_capsule_dist < capsule_dist:
                    features['further-from-capsule'] = 1.0

        ally_pos =  state.data.agentStates[(self.agent.index+2)%4].getPosition()
        ally_dist = self.agent.getMazeDistance(next_pos, ally_pos)
        #features['ally-dist'] = float(ally_dist) / (walls.width * walls.height)


        return features

class QLearningAgent:
    def __init__(self, index, is_training, alpha=.5, epsilon=0.10, gamma=1.0, numTraining = 10):
        self.discount = float(gamma)
        self.numTraining = int(numTraining)
        if not is_training:
            self.alpha = 0.0
            self.epsilon = 0.0
        else:
            self.alpha = float(alpha)
            self.epsilon = float(epsilon)
        self.index = index

        self.values = util.Counter() # A Counter is a dict with default 0

    def computeValueFromQValues(self, state):
        """
          Returns max_action Q(state,action)
          where the max is over legal actions.  Note that if
          there are no legal actions, which is the case at the
          terminal state, you should return a value of 0.0.
        """
        return self.computeFromQValues(state)[0]       

    def computeActionFromQValues(self, state):
        """
          Compute the best action to take in a state.  Note that if there
          are no legal actions, which is the case at the terminal state,
          you should return a random one.
        """
        return random.choice(self.computeFromQValues(state)[1]) 

    def computeFromQValues(self, state):
        legalActions = state.getLegalActions(self.index)
        max_action = (0, ['eRror'])
        if len(legalActions) > 0:
            max_action = (-999999, [random.choice(legalActions)])
        for action in legalActions:
            val = self.getQValue(state, action)
            if max_action[0] == val:
                max_action[1].append(action)
            else:
                max_action = max(max_action, (val, [action]))
        return max_action

    def getAction(self, state):
        """
          Compute the action to take in the current state.  With
          probability self.epsilon, we should take a random action and
          take the best policy action otherwise.  Note that if there are
          no legal actions, which is the case at the terminal state, you
          should choose random as the action.

        """
        # Pick Action
        legalActions = state.getLegalActions(self.index)
        action = random.choice(legalActions)
        if util.flipCoin(self.epsilon):
            return action

        return self.getPolicy(state)
        max_choice = (0, action)
        for action in legalActions:
            max_choice = max((self.getQValue(state, action), action), max_choice)
        return max_choice[1]

    def getPolicy(self, state):
        return self.computeActionFromQValues(state)

    def getValue(self, state):
        return self.computeValueFromQValues(state)


class PacmanQAgent(QLearningAgent):
    "Exactly the same as QLearningAgent, but with different default parameters"

    def __init__(self, index, is_training):
        """
        These default parameters can be changed from the pacman.py command line.
        For example, to change the exploration rate, try:
            python pacman.py -p PacmanQLearningAgent -a epsilon=0.1

        alpha    - learning rate
        epsilon  - exploration rate
        gamma    - discount factor
        numTraining - number of training episodes, i.e. no learning after these many episodes
        """
        self.index = index
        QLearningAgent.__init__(self, index, is_training)

    def getAction(self, state):
        """
        Simply calls the getAction method of QLearningAgent and then
        informs parent of action for Pacman.  Do not change or remove this
        method.
        """
        action = QLearningAgent.getAction(self,state)
        #self.doAction(state,action)
        return action


class ApproximateQAgent(PacmanQAgent):
    def __init__(self, agent, index, featExtractor, weights={}, is_training=True):
        self.featExtractor = featExtractor 
        self.agent = agent

        """
        is_training = False
        if len(weights) == 0:
            weights = util.Counter()
            is_training = True
        """
        if len(weights) == 0:
            weights = util.Counter()
        self.weights = weights

        self.weight_counts = util.Counter()

        PacmanQAgent.__init__(self, index, is_training)

    def getWeights(self):
        return self.weights

    def getQValue(self, state, action):
        """
          Should return Q(state,action) = w * featureVector
          where * is the dotProduct operator
        """
        features = self.featExtractor.getFeatures(state,action)
        q_sum = 0
        for key in features:
            q_sum += features[key] * self.weights[key]
        return q_sum

    def update(self, state, action, nextState, reward):
        """
           Should update your weights based on transition
        """
        #if self.agent.index == 1:
            #print "reward: ", reward, state.getAgentPosition(self.agent.index), nextState.getAgentPosition(self.agent.index)
        features = self.featExtractor.getFeatures(state,action)
        next_state_q = self.computeValueFromQValues(nextState)
        state_q = self.getQValue(state, action)
        for key in features:
            alpha = self.alpha #/(1.0+ .0001*self.weight_counts[key])
            #alpha = max(.01, alpha)
            diff = (reward + self.discount * next_state_q) - state_q
            #if self.agent.index == 1:
                #print key, features[key], self.weights[key]
            self.weights[key] += alpha*diff*features[key]
            #if self.agent.index == 1:
                #print self.weights[key]
            #self.weight_counts[key] += 1

    def final(self, state):
        "Called at the end of each game."
        # call the super-class final method
        #PacmanQAgent.final(self, state)

        # did we finish training?
        #if self.episodesSoFar == self.numTraining:
            # you might want to print your weights here for debugging
        #print self.index, self.weights
        pass
