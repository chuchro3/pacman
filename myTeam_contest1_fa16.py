# myTeam.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).

"""
Description of ZuzuAgent for Contest 1

This agent attempts to solve this problem by estimating a solution to the complete state space represented by the search problem.

First, attempt to remove the two food pellets to be ignored by considering the distance they are from a return point as well as their distance to other food.

Second, use kmeans clustering to assign sets of points to clusters. Simulated annealing is used to generate initial cluster centers by estimating finding k points whose sum of distances apart is the greatest.

Third, calculate how many steps it would take to collect each cluster, starting at the cluster center. This will be used as edge weights in the following problem.

Fourth, solve the optimal order for both agents to visit the clusters. This is a state space represented by agent1 position, agent2 position, and set of unvisited clusters. Starting clusters are starting positions, and goal clusters are one of the possible return positions (generated once cluster set is empty). Edge weights are the cost to collect cluster (as calculated above) + cost to travel from one cluster center to another.
The total running cost that is recorded is a modified tuple of two running sums of path costs (one for each agent), where the compare function is overriden to consider the greater of the two total path costs.

Fifth, once each agent has a sequence of clusters to collect, calculate the final path by iteratively picking up all the food in one cluster + one food in the next cluster, and concatenating the sub-paths together.

Lastly, since this is just an estimate of the optimal solution and the clustering algorithms generate different clusters per iteration, we continue to repeat the algorithm until the time limit is reached, keeping track of the best solution found.

Agent designed by: Robert Chuchro
"""

from captureAgents import CaptureAgent
import random, time, util
from game import Directions, Actions, Grid
import game
import math
import time

#################
# Team creation #
#################

def createTeam(firstIndex, secondIndex, isRed,
               first = 'ZuzuAgent', second = 'ZuzuAgent'):
    """
    This function should return a list of two agents that will form the
    team, initialized using firstIndex and secondIndex as their agent
    index numbers.    isRed is True if the red team is being created, and
    will be False if the blue team is being created.

    As a potentially helpful development aid, this function can take
    additional string-valued keyword arguments ("first" and "second" are
    such arguments in the case of this function), which will come from
    the --redOpts and --blueOpts command-line arguments to capture.py.
    For the nightly contest, however, your team will be created without
    any extra arguments, so you should make sure that the default
    behavior is what you want for the nightly contest.
    """
    shared_dic = {}
    # The following line is an example only; feel free to change it.
    return [eval(first)(firstIndex, shared_dic), eval(second)(secondIndex, shared_dic)]

#returns a dictionary of cluster points : corresponding points in the cluster
def kMeans(distancer, layout, food_list, prev_centers, seen=set()):
    new_centers = []
    #tuple of running sum of x, running sum of y, count, list of points
    center_pts = {key: tuple((0, 0, 0, [])) for key in prev_centers}
    #mapping of new centroids to old centroids for reference
    center_pt_mapping = {}

    #iterate through every point
    for fud_pt in food_list:
        min_dist = (999999, (-1, -1))
        #iterate over k center points
        for ctr_pt in prev_centers:
            ctr_pt = nearestPoint(ctr_pt, layout)
            dist = distancer.getDistance(ctr_pt, fud_pt)
            min_dist = min(min_dist, (dist, ctr_pt))
            
        #maintain a rolling sum of all the points in a cluster
        nearest_pt = min_dist[1]
        rolling_pt = center_pts[nearest_pt][:-1]
        pt_list = center_pts[nearest_pt][-1]
        pt_list.append(fud_pt)
        new_rolling_pt = map(sum, zip(fud_pt+(1,), rolling_pt))
        new_rolling_pt.append(pt_list)
        center_pts[nearest_pt] = new_rolling_pt

    #calculate the avarage x,y of each point per cluster
    for ctr_pt in prev_centers:
        rolling_pt = center_pts[ctr_pt]
        count = rolling_pt[2] + .01
        new_center = (1.0 * rolling_pt[0] / count, 1.0 * rolling_pt[1] / count)
        new_center = nearestPoint(new_center, layout)
        layout.walls[new_center[0]][new_center[1]] = True
        new_centers.append(new_center)
        center_pt_mapping[new_center] = ctr_pt
    layout.walls = restoreLayout(layout, new_centers)

    #check for convergence
    #convergence occurs when none of the cluster centers change position after an iteration
    #print new_centers
    if hasConverged(new_centers, seen):
        center_dic = {key: center_pts[center_pt_mapping[key]][-1] for key in new_centers}
        return center_dic
    seen.add(tuple(new_centers))
    return kMeans(distancer, layout, food_list, new_centers, seen)

def hasConverged(new_centers, seen):
    if tuple(new_centers) in seen:
        return True
    return False
        

def nearestPoint(pt, layout):
    x,y = util.nearestPoint(pt)
    q = util.Queue()
    q.push( (x,y) )
    seen = set()
    while not q.isEmpty():
        x,y = q.pop()
        if (x,y) in seen or x < 0 or y < 0 or x >= layout.width or y >= layout.height:
            continue
        if layout.isWall((x,y)):
            neighbors = [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]
            #randomize the order we visit potential neighbors
            random.shuffle(neighbors)
            for neighbor in neighbors:
                q.push(neighbor)
        else:
            break
    return x,y

def restoreLayout(layout, pts):
    for x,y in pts:
        layout.walls[x][y] = False
    return layout.walls
        
# returns the intersection of the two lists
def listIntersect(list1, list2):
    set1 = set(list1)
    set2 = set(list2)
    return list(set1 & set2)

##########
# Agents #
##########

class ZuzuAgent(CaptureAgent, object):
    
    def __init__( self, index, shared_dic):
        self.shared_dic = shared_dic
        super(ZuzuAgent, self).__init__(index)

    def registerInitialState(self, gameState):
        CaptureAgent.registerInitialState(self, gameState)
        time_started = time.time()
        #print vars(gameState)
        #print vars(gameState.data)

        self.starting_game_state = gameState.deepCopy()
        self.starting_pos = self.starting_game_state.data.agentStates[self.index].getPosition()
        self.ally_starting_pos = self.starting_game_state.data.agentStates[(self.index + 2) % 4].getPosition()

        self.gameState = gameState.deepCopy()
        side = 0.5
        if self.red:
            side = -0.5
        self.returnX = int(self.gameState.data.layout.width / 2.0 + side)
        self.return_pos = self.getReturnPos()
        self.path_idx = 0

        #initializing tasks needed by just the first agent
        if self.index < 2:
            self.shared_dic.clear()
            self.shared_dic['paths'] = {}
            self.shared_dic['path_length'] = 999999
            
            #time1 = time.time()
            self.shared_dic['28_food'] = self.removeFurthestFoodFromCenter()
            #self.gameState.data.food = self.removeFurthestFoodFromRest()
            #time2 = time.time()
            #time_taken = (time2-time1)
            #print "time taken to remove furthest 2: ", time_taken
            #time1 = time.time()
        else:
            self.shared_dic['28_food'] = self.removeFurthestFoodFromNeighbors()

        
        time_limit = 13.0
        num_iterations = 0
        while time.time() - time_started < time_limit:
            num_iterations += 1
            #print "current duration:", time.time()-time_started

            #reset game state
            self.gameState = self.starting_game_state.deepCopy()
            self.gameState.data.food = self.shared_dic['28_food'].copy()

            #time1 = time.time()
            #generate k clusters of food
            max_k = 8
            if num_iterations > 1:
                max_k = 7
            self.shared_dic['clusters'] = self.kMeansClustering(self.getFood(self.gameState).asList(), gameState.data.layout, max_k=max_k)
                    
            #time2 = time.time()
            #time_taken = (time2-time1)
            #print "time taken to generate clusters: ", time_taken
            #time1 = time.time()
            
            #assign edge weights for collecting each of the clusters
            self.shared_dic['cluster_weights'] = util.Counter() 
            cluster_weights = self.weighClusters(self.shared_dic['clusters'], time_started)
            if cluster_weights is None:
                break
            self.shared_dic['cluster_weights'].update(cluster_weights)
            #print "cluster weights:", self.shared_dic['cluster_weights']

            #time2 = time.time()
            #time_taken = (time2-time1)
            #print "time taken for cluster weights: ", time_taken
            #time1= time.time()
            
            self.shared_dic['cluster_paths'] = self.calculateClusterTraversal(time_started)
            if self.shared_dic['cluster_paths'] is None:
                break
        
            #time2 = time.time()
            #time_taken = (time2-time1)
            #print "time taken for cluster paths: ", time_taken
            
            starting_pos_list = [self.ally_starting_pos, self.starting_pos]
            #print "starting positions: ", starting_pos_list
            self.shared_dic['cluster_paths'] = self.trimClusterPaths(starting_pos_list)
            #print "cluster_paths", self.shared_dic['cluster_paths']

            paths = {}
            idxs = [(self.index + 2) % 4, self.index]
            for i in range(len(self.shared_dic['cluster_paths'])):
                idx = idxs[i]
                start_pos = starting_pos_list[i]
                cluster_path = self.shared_dic['cluster_paths'][i]
                
                #print "before", cluster_path
                merge_size = 9
                cluster_path = self.mergeClusters(cluster_path, merge_size)
                #print "after", cluster_path
                
                #time1 = time.time()
                path = self.traverseClusters(cluster_path, idx, time_started)
                if path is None:
                    return
                #time2 = time.time()
                #time_taken = (time2-time1)
                #print "time taken for complete path", idx, ":", time_taken
                #target = open("timing_data.txt", 'a')
                #target.write(str(size)+", "+str(furthest)+", "+str(time_taken))
                #target.write("\n")
                #target.close()
                
                paths[idx] = path
                #print "final path length: ", idx, len(complete_path)
            max_path = -1
            for path in paths.values():
                max_path = max(max_path, len(path))
            if max_path < self.shared_dic['path_length']:
                #print "found better solution: ", max_path
                self.shared_dic['paths'] = paths
                self.shared_dic['path_length'] = max_path

        #time_finished = time.time()
        #total_time_taken = time_finished - time_started
        #print "time taken for iterating", num_iterations, "solutions: ", total_time_taken
        """
        target = open("data/path_length_data.txt", 'a')
        max_path = -1
        for path in self.shared_dic['paths'].values():
            max_path = max(max_path, len(path))
        target.write(str(total_time_taken)+", "+str(max_path))
        target.write("\n")
        target.close()
        """

    def chooseAction(self, gameState):
        #print "idx: ", self.path_idx
        path = self.shared_dic['paths'][self.index]
        if self.path_idx >= len(path):               
            actions = gameState.getLegalActions(self.index)
            return random.choice(actions)
        action = path[self.path_idx]
        #print "action: ", action
        self.path_idx += 1
        return action  


    def final(self, gameState):
        self.shared_dic.clear()
        super(ZuzuAgent, self).final(gameState)

    #calculates starting cluster estimates, then calls k means clustering
    def kMeansClustering(self, food_list, layout, min_k=5, max_k=8):
        trials_per_k = 8
        best_dic = (999999, {})
        best_cluster_size = 999999
        for k in range(min_k, max_k+1, 1):
            for trial in range(trials_per_k):
                cluster_estimates = self.chooseStartingCentroids(k, food_list, layout)
                #print "estimates k =", k, "trial # ", trial
                #print cluster_estimates
                
                cluster_dic = kMeans(self.distancer, layout, food_list, cluster_estimates)
                most = -1
                largest_cluster = -1
                smallest_cluster = 999999
                for centroid in cluster_dic.keys():
                    cluster = cluster_dic[centroid]
                    furthest = self.calculateFurthestDistanceFrom(centroid, cluster)
                    largest_cluster = max(largest_cluster, len(cluster))
                    smallest_cluster = min(smallest_cluster, len(cluster))
                    most = max(most, furthest)
                #estimate a score for the cluster
                cluster_score = 1.175 * most / 9 - 0.6 * k / 6
                #keep track of the best clustering we've seen
                if (cluster_score, cluster_dic) < best_dic:
                    best_dic = (cluster_score, cluster_dic)
                    best_most = most
                    best_k = k
                    best_cluster_size = largest_cluster
        #print "best cluster (furthest, k, largest cluster): ", best_most, best_k, best_cluster_size
        return best_dic[-1]

        """
        chosen_dic = random.choice(all_dics)
        print "chosen cluster (furthest, k, largest cluster, smallest cluster): ", chosen_dic[:-1]
        target = open("data/cluster_structure_data.txt", 'a')
        target.write(str(chosen_dic[0])+", "+str(chosen_dic[1])+", "+str(chosen_dic[2])+", "+str(chosen_dic[3]))
        target.write("\n")
        target.close()
        return chosen_dic[-1]
        """

    def chooseStartingCentroids(self, k, food_list, layout):
        return self.simulatedAnnealing(k, food_list)


    #simulated annealing used to approximate choosing k starting centroids
    #with the greatest total distance apart from eachother
    def simulatedAnnealing(self, k, nodes):
        n = len(nodes)
        random.shuffle(nodes)

        #init subset to first k elements
        curr_k = nodes[0:k]
        curr_unused = nodes[k:]
        curr_distances = self.calculateDistances(curr_k)

        #simulated annealing params
        temp = 1.0 * (n + 1.0 / 2)
        cooling_rate = .991
        #iterations = 10 * n * n * k
        iterations = 550

        for i in range(iterations):
            #print "current centroids:", curr_k
            proposed_k = list(curr_k)
            proposed_unused = list(curr_unused)

            #pick two random idxs to swap
            k_idx = random.randint(0,k-1)
            unused_idx = random.randint(0,n-k-1)

            #swap the two elements in the proposed subset
            #print "proposed swap:", proposed_k[k_idx], proposed_unused[unused_idx]
            tmp = proposed_k[k_idx]
            proposed_k[k_idx] = proposed_unused[unused_idx]
            proposed_unused[unused_idx] = tmp
            
            proposed_distances = self.calculateDistances(proposed_k)
            
            potential_change = math.exp((-1.0/temp) * math.pow(curr_distances / proposed_distances, 4))
            #print "potential change:", potential_change, curr_distances, proposed_distances

            if potential_change > 1.0 or potential_change >= random.random():
                curr_k = proposed_k
                curr_unused = proposed_unused
                curr_distances = proposed_distances

            temp *= cooling_rate
        #print "last potential change val:", potential_change

        return curr_k

    # assigns a weight to each cluster
    #
    # weight is determined by how many steps are required to collect all the food in the cluster,
    # starting at the centroid
    #
    # returns a dictionary of centroids : weight value
    def weighClusters(self, clusters, timer):
        cluster_weights = {}

        for centroid in clusters.keys():
            cluster_pts = clusters[centroid]
            self.calculateFoodDistances(cluster_pts)
            self.problem = Problem(self.gameState, centroid, cluster_pts, self.food_distances, self.index, self.red, self.return_pos)
            path = aStarSearch(self, longestMoveHeuristic, timer=timer)
            if path is None:
                return None
            cost = len(path)
            cluster_weights[centroid] = cost

        return cluster_weights

    # solves a search problem which determines the order of clusters for each agent to visit
    def calculateClusterTraversal(self, timer):
        start_pos1 = self.gameState.data.agentStates[self.index].getPosition()
        start_pos2 = self.gameState.data.agentStates[(self.index + 2) % 4].getPosition()
        clusters = self.shared_dic['cluster_weights'].keys()
        cluster_costs = self.shared_dic['cluster_weights']
        goals = self.return_pos

        self.problem = ClusterProblem(start_pos1, start_pos2, clusters, cluster_costs, goals, self.distancer)
        path = aStarSearch(self, noneHeuristic, debug=False, timer=timer)
        return path

    # attempts to merge clusters in sequence into a larger cluster
    # updates the shared cluster dictionary as clusters are merged
    def mergeClusters(self, cluster_path, max_size=9):
        new_cluster_path = []
        prev_centroid = cluster_path[0]
        for i in range(1, len(cluster_path)):
            centroid = cluster_path[i]
            cluster_pts = self.shared_dic['clusters'][centroid]
            size = len(cluster_pts)
            
            prev_cluster_pts = self.shared_dic['clusters'][prev_centroid]
            prev_size = len(prev_cluster_pts)

            if size + prev_size <= max_size:
                new_cluster = cluster_pts + prev_cluster_pts
                new_centroid = prev_centroid
                self.shared_dic['clusters'][prev_centroid] = new_cluster
            else:
                new_cluster_path.append(prev_centroid)
                prev_centroid = centroid
        new_cluster_path.append(prev_centroid)
        return new_cluster_path

    # calculate paths for traversing to each cluster and picking up all the food pellets
    # by solving a small search problem to pick up each cluster
    def traverseClusters(self, cluster_path, index, timer):
        game_state = self.gameState
        self.calculateFoodDistances(self.getFood(game_state).asList() + cluster_path)
        complete_path = []
        for i in range(len(cluster_path)):
            cluster = cluster_path[i]
            next_cluster_pts = self.return_pos
            if i < len(cluster_path)-1:
                next_cluster_pts = listIntersect(self.shared_dic['clusters'][cluster_path[i+1]], self.getFood(game_state).asList()) 
            food = listIntersect(self.shared_dic['clusters'][cluster], self.getFood(game_state).asList()) 
            start_pos = game_state.data.agentStates[index].getPosition()
            self.problem = ClusterTraversalProblem(game_state, start_pos, food, self.food_distances, index, self.red, next_cluster_pts)
            path = aStarSearch(self, longestMoveHeuristic, timer=timer)
            if path is None:
                return None
            #print "nodes expanded:", self.problem.getExpandedCount()
            game_state = self.simulateMoves(path, game_state, index)
            complete_path += path
        self.gameState = game_state
        return complete_path

    # extracts the individual paths from the cluster path traversal problem
    # removes the last element since it is a goal position
    # removes duplicates
    # removes a starting position if it exists (at the start of the path)
    def trimClusterPaths(self, starting_pos_list):
        cluster_path0 = [x[0] for x in self.shared_dic['cluster_paths']]
        cluster_path1 = [x[1] for x in self.shared_dic['cluster_paths']]
        cluster_paths = [cluster_path0, cluster_path1]
        
        trimmed_paths = []
        for i in range(len(cluster_paths)):
            cluster_path = cluster_paths[i]
            cluster_path = cluster_path[:-1]
            seen = set(starting_pos_list)
            for centroid in list(cluster_path):
                if centroid in seen:
                    cluster_path.remove(centroid)
                else:    
                    seen.add(centroid)
            trimmed_paths.append(cluster_path)
        return trimmed_paths

    # deterimines all the return positions along the vertical divider of the map
    def getReturnPos(self):
        state = self.gameState
        walls = state.getWalls()
        pts = []
        side = 0.5
        if self.red:
            side = -0.5
        x = state.data.layout.width / 2.0 + side
        for y in range(1, state.data.layout.height):
            if not walls[int(x)][int(y)]:
                pts.append( (int(x), int(y)) )
        return pts

    #calculates the sum of the distances between each node
    def calculateDistances(self, nodes):
        sum = 0
        max_dist = 0
        min_dist = 999999
        for i in range(len(nodes)):
            for j in range(i+1, len(nodes)):
                dist = self.getMazeDistance(nodes[i], nodes[j])
                #sum += math.sqrt(dist)
                sum += dist
                max_dist = max(max_dist, dist)
                min_dist = min(min_dist, dist)
        sum -= len(nodes) / 2.0 * (max_dist - min_dist)
        return sum

    # calculate the sum of the distance between the given point and every point in the list
    def calculateDistancesFrom(self, pos, pt_list):
        dist_sum = 0
        for pt in pt_list:
            dist = self.getMazeDistance(pos, pt)
            dist_sum += dist
        return dist_sum

    #returns the point in the given list that is nearest to the given position
    def calculateNearestPoint(self, pos, pt_list):
        nearest = (999999, (-1, -1))
        for pt in pt_list:
            dist = self.getMazeDistance(pos, pt)
            nearest = min(nearest, (dist, pt))
        return nearest[1]

    # calculate the nearest distance between two points in the given list
    def calculateNearestDistance(self, pos, pt_list):
        nearest = 999999
        for pt in pt_list:
            dist = self.getMazeDistance(pos, pt)
            nearest = min(nearest, dist)
        return nearest
    
    # calculate the furthest distance between two points in the given list
    def calculateFurthestDistance(self, pt_list):
        furthest = -1
        for pt in pt_list:
            dist = self.calculateFurthestDistanceFrom(pt, pt_list)
            furthest = max(furthest, dist)
        return furthest
    
    # calculates the furthest distance from a point in the given list to the given position
    def calculateFurthestDistanceFrom(self, pos, pt_list):
        furthest = -1
        for pt in pt_list:
            dist = self.getMazeDistance(pos, pt)
            furthest = max(furthest, dist)
        return furthest

    # remove the 2 food that are the furthest from the rest of the food
    def removeFurthestFoodFromRest(self):
        food_list = self.getFood(self.gameState).asList()
        largest = (-1, (-1, -1))
        largest2 = (-1, (-1, -1))
        #from each food position, find sum distance from other food
        for food_pos in food_list:
            sum_dist = self.calculateDistancesFrom(food_pos, food_list)
            dist = (sum_dist, food_pos)
            if dist > largest:
                largest2 = largest
                largest = dist
            elif dist > largest2:
                largest2 = dist 
        #remove the two furthest foods from the grid
        grid = self.gameState.data.food.copy()
        x1,y1 = largest[1]
        x2,y2 = largest2[1]
        #print "furthest foods: ", largest[1], largest2[1]
        grid[x1][y1] = False
        grid[x2][y2] = False
        return grid
        
    #since we need to collect all but 2 food pellets,
    #remove the 2 that are furthest from the center
    def removeFurthestFoodFromCenter(self):
        food_list = self.getFood(self.gameState).asList()
        largest = (-1, (-1, -1))
        largest2 = (-1, (-1, -1))
        #from each food position, find furthest return distance
        for food_pos in food_list:
            path = self.calculateReturnPath(self.gameState, food_pos, self.index)
            dist = (len(path), food_pos)
            if dist > largest:
                largest2 = largest
                largest = dist
            elif dist > largest2:
                largest2 = dist 
        #remove the two furthest foods from the grid
        grid = self.gameState.data.food.copy()
        x1,y1 = largest[1]
        x2,y2 = largest2[1]
        #print "furthest foods center: ", largest[1], largest2[1]
        grid[x1][y1] = False
        grid[x2][y2] = False
        return grid

    #since we need to collect all but 2 food pellets,
    #remove the 2 that are furthest from its two closest neighbors (including returning)
    def removeFurthestFoodFromNeighbors(self):
        food_list = self.getFood(self.gameState).asList()
        largest = (-1, (-1, -1), (-1, -1))
        #from each food position, find furthest return distance
        food_copy = list(food_list)
        for i in range(len(food_list)):
            food_pos1 = food_list[i]
            food_copy.remove(food_pos1)
            for j in range(i+1, len(food_list)):
                food_pos2 = food_list[j]
                #print "points", food_pos1, food_pos2
                food_copy.remove(food_pos2)
                nearest_pt1 = self.calculateNearestPoint(food_pos1, food_copy)
                #print "nearest pt1:", nearest_pt1
                nearest_dist1 = self.getMazeDistance(nearest_pt1, food_pos1)
                nearest_pt2 = self.calculateNearestPoint(food_pos2, food_copy)
                #print "nearest pt2:", nearest_pt2
                nearest_dist2 = self.getMazeDistance(nearest_pt2, food_pos2)
          
                food_copy.remove(nearest_pt1)
                next_closest_dist1 = self.calculateNearestDistance(food_pos1, food_copy+self.return_pos)
                food_copy.append(nearest_pt1)
                food_copy.remove(nearest_pt2)
                next_closest_dist2 = self.calculateNearestDistance(food_pos2, food_copy+self.return_pos)
                food_copy.append(nearest_pt2)
                
                total_dist = nearest_dist1 + nearest_dist2 + next_closest_dist1 + next_closest_dist2
                #print "return path1:", nearest_dist1
                #print "return path2:", nearest_dist2
                #print "next closest1:", next_closest_dist1
                #print "next closest2:", next_closest_dist2
                #print "total dist:", total_dist
                dist = (total_dist, food_pos1, food_pos2)
                largest = max(largest, dist)

                food_copy.append(food_pos2)
            food_copy.append(food_pos1)
        #remove the two furthest foods from the grid
        grid = self.gameState.data.food.copy()
        x1,y1 = largest[1]
        x2,y2 = largest[2]
        #print "furthest foods neigh: ", largest[1], largest[2]
        grid[x1][y1] = False
        grid[x2][y2] = False
        return grid

    def getFoodGrid(self, index, gameState, cluster_list):
        grid = Grid(gameState.data.food.width, gameState.data.food.height)
        food_list = cluster_list[0]
        if index > 1:
            food_list = cluster_list[1]

        for fud_pt in food_list:
            x = fud_pt[0]
            y = fud_pt[1]
            grid[x][y] = True
        #print "num food: ", grid.count()
        return grid

    # returns a game state after making the given moves
    def simulateMoves(self, path, game_state, index):
        for direction in path:
            vector = Actions.directionToVector(direction)
            config = game_state.data.agentStates[index].configuration.generateSuccessor(vector)
            game_state.data.agentStates[index].configuration = config
            pos = config.getPosition()
            game_state.data.food[int(pos[0])][int(pos[1])] = False
        return game_state

    def calculateFoodDistances(self, foodList, minCount=0):
        self.food_distances = []
        for i in range(0, len(foodList)):
            for j in range(i+1, len(foodList)):
                dist = self.getMazeDistance(foodList[i], foodList[j])
                self.food_distances.append( (dist, foodList[i], foodList[j]) )
        self.food_distances = sorted(self.food_distances, key=lambda x: x[0], reverse=True)

    def calculateReturnPath(self, gameState, start_pos, index):
        self.problem = ReturnProblem(self.gameState, start_pos, self.getFood(gameState).asList(), None, index, self.red, self.return_pos)
        path = aStarSearch(self, returnDistance)
        return path

class aStarStrategy:
    def __init__(self, agent, heuristic):
        self.agent = agent
        self.heuristic = heuristic
    
    def strategy(self, node):
        return node[2] + self.heuristic(node[0], self.agent)

def aStarSearch(agent, heuristic, debug=False, timer=None):
    """Search the node that has the lowest combined cost and heuristic first."""
    a_star = aStarStrategy(agent, heuristic)
    fringe = util.PriorityQueueWithFunction(a_star.strategy)
    return search(agent.problem, fringe, debug, timer)

def search(problem, fringe, debug=False, timer=None):
    visited = set()
    fringe.push([problem.getStartState(), [], problem.getStartCost()])
    while not fringe.isEmpty():
        #time limit reached, need to exit
        if timer is not None and time.time() - timer > 14.2:
            #print "time threshold exceeded"
            return None
        node = fringe.pop()
        state = node[0]
        path = node[1]
        cost = node[2]
        if problem.isGoalState(state):
            #if debug:
            #    print "solution:"
            #    print "state:", state
            #    print "cost:", cost
            #    print "path:", path
            return path
        if state not in visited:
            #if debug:
            #    print "state:", state
            #    print "cost:", cost
            visited.add(state)
            for child_node in problem.getSuccessors(state):
                new_path = list(path)
                new_path.append(child_node[1])
                new_cost = cost + child_node[2]
                fringe.push((child_node[0], new_path, new_cost))
    return []

def noneHeuristic(state, agent):
    return agent.problem.getStartCost()

def longestMoveHeuristic(state, agent):
    foodList = state[1]
    food_heur0 = longestMoveHeuristicHelper(state[0], foodList, agent)
    return_heur0 = returnDistance(state, agent)
    side_heur0 = calculateSideHeuristic(state[0], agent)
    #print "heuristics: ", food_heur0, return_heur0, side_heur0
    return food_heur0 + side_heur0 + return_heur0

def longestMoveHeuristicHelper(position, foodList, agent):
    """
    A* heuristic from project1
    """
    x = int(position[0])
    y = int(position[1])
    position = (x, y)
    if len(foodList) <= 2:
        return 0
    #idx = agent.problem.minCount
    idx = 0
    for i in range(idx, len(agent.food_distances)):
        truple = agent.food_distances[i]
        if truple[1] not in foodList or truple[2] not in foodList:
            idx += 1
        else:
            break

    if idx >= len(agent.food_distances):
        if len(foodList) == 1:
            return agent.getMazeDistance(position, foodList[0])
        else:
            return 0
    maxDist,food1,food2 = agent.food_distances[idx]
    
    dist1 = agent.getMazeDistance(position, food1)
    dist2 = agent.getMazeDistance(position, food2)
    shortDist = min(dist1, dist2)
    return maxDist + shortDist

def returnDistance(state, agent):
    pos = state[0]
    minDist = 999999
    for x,y in agent.problem.end_pos_list:
        dist = 999999
        if not agent.problem.walls[x][y]:
            dist = agent.getMazeDistance(pos, (x, y))
        if dist < minDist:
            minDist = dist
    return minDist

# way too expensive
def miniProblem(state, agent):
    prob = agent.problem
    pos = state[0]
    food = state[1]
    num_food = len(food)
    #dont use this heuristic for small problems
    if num_food - prob.minCount < 12:
        return 0
    #min_count = num_food * .5
    min_count = num_food - 2
    mini_prob = Problem(prob.startingGameState, pos, food, prob.food_distances, prob.idx, prob.is_red, prob.end_pos_list, min_count)
    agent.problem = mini_prob
    path = aStarSearch(agent, longestMoveHeuristic)
    agent.problem = prob
    return len(path)


def calculateSideHeuristic(pos, agent):
    if (pos[0] - agent.returnX) * agent.problem.xDiff >= 3:
        return 999999
    return 0

class Problem(object):

    def __init__(self, startingGameState, start_pos, food, food_distances, idx, is_red, end_pos_list, minCount=0):
        #print startingGameState.data.agentStates[idx].getPosition()
        #self.start = (startingGameState.data.agentStates[idx].getPosition(), food)
        self.start = (start_pos, tuple(food))
        self.walls = startingGameState.getWalls()
        self.startingGameState = startingGameState
        self.food_distances = food_distances
        self._expanded = 0
        #print "width: ", startingGameState.data.layout.width
        #print "height: ", startingGameState.data.layout.height
        self.xDiff = 1 #-1 for red, 1 for blue
        if is_red:
            self.xDiff = -1
        self.end_pos_list = end_pos_list
        self.minCount = minCount
        
        self.start_pos = start_pos
        self.food = food
        self.idx = idx
        self.is_red = is_red

    def copy(self):
        new_problem = Problem(self.startingGameState, self.start_pos, self.food, self.food_distances, self.idx, self.is_red, self.end_pos_list, self.minCount)
        return new_problem

    def getExpandedCount(self):
        return self._expanded

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        pos = self.start[0]
        food = list(self.start[1])
        #handle if we are starting on a food
        if pos in food:
            food.remove(pos)
            start = list(self.start)
            start[1] = tuple(food)
            self.start = tuple(start)
        return self.start

    def getStartCost(self):
        """
        Used to define different types of 0s to initiate starting cost
        """
        return 0

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        if len(state[1]) <= self.minCount:
            #print state[0]
            #print state[1]
            #print self.hasReturned(state[0][0])
            #print self.hasReturned(state[1][0])
            return True
        return False
    
 
    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        successors = []
        self._expanded += 1 
        for direction in [Directions.NORTH, Directions.SOUTH, Directions.EAST, Directions.WEST]:
            x,y = state[0]
            dx, dy = Actions.directionToVector(direction)
            nextx, nexty = int(x + dx), int(y + dy)
            if not self.walls[nextx][nexty]:

                        nextFood = list(state[1])
                        #nextFood[nextx][nexty] = False
                        if (nextx, nexty) in nextFood:
                            nextFood.remove( (nextx, nexty) )
                        successors.append( ( ((nextx, nexty), tuple(nextFood)), direction, 1) )
        return successors

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        return len(actions)

class ClusterTraversalProblem(Problem):

    def isGoalState(self, state):
        pos = state[0]
        if super(ClusterTraversalProblem, self).isGoalState(state) and pos in self.end_pos_list:
            return True
        return False


class ReturnProblem(Problem):
    def isGoalState(self, state):
        #print state[0], self.hasReturned(state[0][0]), self.endX
        return self.hasReturned(state[0][0])

    def hasReturned(self, x):
        endX = self.startingGameState.data.layout.width/2.0 + self.xDiff/2.0
        endX = int(endX)
        return (x - endX) * self.xDiff >= 0

class ClusterProblem(Problem):
    def __init__(self, start_pos1, start_pos2, clusters, cluster_costs, goals, distancer):
        self.start = (start_pos1, start_pos2, tuple(clusters))
        self.goals = goals
        self.cluster_costs = cluster_costs
        self.distancer = distancer

    def isGoalState(self, state):
        pos1, pos2, clusters = state
        if pos1 in self.goals and pos2 in self.goals:
            return True
        return False

    def getStartCost(self):
        return CostTuple( (0,0) )

    def getSuccessors(self, state):
        successors = []
        pos1, pos2, clusters = state
        
        #self._expanded += 1 
        # once we are at a state where all clusters have been visited, 
        # successors are only potential return locations
        if len(clusters) == 0:
            clusters = self.goals
        else:
            clusters = list(clusters)
            clusters.append(None) #represents no action by one agent

        for i in range(len(clusters)):
            cluster1 = clusters[i]
            cost1 = 0
            if cluster1 is None:
                cluster1 = pos1
            else:
                dist1 = self.distancer.getDistance(pos1, cluster1)
                cost1 = dist1 + self.cluster_costs[cluster1]
            for j in range(len(clusters)):
                cluster2 = clusters[j]
                cost2 = 0
                #avoid collecting the same cluster
                if cluster2 == cluster1 and cluster2 not in self.goals:
                    continue
                if cluster2 is None:
                    cluster2 = pos2
                else:
                    dist2 = self.distancer.getDistance(pos2, cluster2)
                    cost2 = dist2 + self.cluster_costs[cluster2]
                cost = [cost1, cost2]
                #sort by most expensive cost first
                #cost.sort(reverse=True)
                cost = CostTuple( tuple(cost) )
                new_clusters = list(clusters)
                if cluster1 != pos1 and cluster1 not in self.goals:
                    new_clusters.remove(cluster1)
                if cluster2 != pos2 and cluster2 not in self.goals:
                    new_clusters.remove(cluster2)
                if cluster1 not in self.goals:
                    new_clusters.remove(None)
                #print "cluster: ", cluster1, cluster2
                #print "cost: ", cost
                #print "remaining: ", new_clusters
                successors.append( ( (cluster1, cluster2, tuple(new_clusters)), (cluster1, cluster2), cost ) )

        return successors


    #def getCostOfActions(self, actions)

class CostTuple(tuple):
    def __init__(self, tup):
        self._tuple = tup

    # returns a new tuple with by adding together elements of corresponding indices
    def __add__(self, other):
        new_tup = []
        for i in range(len(self._tuple)):
            elem1 = self._tuple[i]
            elem2 = other._tuple[i]
            new_tup.append(elem1 + elem2)
        return CostTuple(tuple(new_tup))

    def __lt__(self, other):
        list1 = list(self._tuple)
        list1.sort(reverse=True)
        list2 = list(other._tuple)
        list2.sort(reverse=True)
        return list1 < list2

    def __le__(self, other):
        list1 = list(self._tuple)
        list1.sort(reverse=True)
        list2 = list(other._tuple)
        list2.sort(reverse=True)
        return list1 <= list2

    def __eq__(self, other):
        list1 = list(self._tuple)
        list1.sort(reverse=True)
        list2 = list(other._tuple)
        list2.sort(reverse=True)
        return list1 == list2

    def __ne__(self, other):
        list1 = list(self._tuple)
        list1.sort(reverse=True)
        list2 = list(other._tuple)
        list2.sort(reverse=True)
        return list1 != list2

    def __gt__(self, other):
        list1 = list(self._tuple)
        list1.sort(reverse=True)
        list2 = list(other._tuple)
        list2.sort(reverse=True)
        return list1 > list2

    def __ge__(self, other):
        list1 = list(self._tuple)
        list1.sort(reverse=True)
        list2 = list(other._tuple)
        list2.sort(reverse=True)
        return list1 >= list2
